const express = require('express');
const controller = require('../controllers/user');
const auth = require('../middleware/authentication');

const router = express.Router();

router.get('/signup', controller.get_signup);

router.post('/signup', controller.post_signup);

router.get('/login', controller.get_login);

router.post('/login', controller.post_login);

router.get('/logout', auth.checkAuth, controller.get_logout);

router.get('/email-confirm/:guid', controller.get_email_confirm_guid);

router.get('/email-confirm', controller.get_email_confirm);

module.exports = router;