const express = require('express');
const auth = require('../middleware/authentication');
const controller = require('../controllers/galaxy');


const router = express.Router();

router.get('/', auth.checkAuth, auth.checkEmpire, controller.get_galaxy);

module.exports = router;