const express = require('express');
const auth = require('../middleware/authentication');
const controller = require('../controllers/race');


const router = express.Router();

router.get('/', auth.checkAuth, auth.checkNoEmpire, controller.get_race);

module.exports = router;