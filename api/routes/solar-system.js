const express = require('express');
const auth = require('../middleware/authentication');
const controller = require('../controllers/solar-system');


const router = express.Router();

router.get('/:id', auth.checkAuth, auth.checkEmpire, controller.get_solar_system);

router.get('/', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, controller.get_planet_solar_system);

module.exports = router;