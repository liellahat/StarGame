const express = require('express');
const auth = require('../middleware/authentication');
const controller = require('../controllers/planet');


const router = express.Router();

router.get('/home', auth.checkAuth, auth.checkEmpire, controller.get_home_planet);

router.get('/:id', auth.checkAuth, auth.checkEmpire, controller.get_planet);

router.get('/population-choice/peace', auth.checkAuth, auth.checkEmpire, controller.get_population_peace);

router.get('/population-choice/slavery', auth.checkAuth, auth.checkEmpire, controller.get_population_slavery);

router.get('/population-choice/extermination', auth.checkAuth, auth.checkEmpire, controller.get_population_extermination);

router.get('/', auth.checkAuth, auth.checkEmpire, controller.get_current_planet);


module.exports = router;