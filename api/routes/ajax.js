const express = require('express');
const auth = require('../middleware/authentication');
const controller = require('../controllers/ajax');


const router = express.Router();

// race
router.get('/race/stats', auth.checkAuth, auth.checkNoEmpire, controller.get_race_stats);

router.post('/race/name-check', auth.checkAuth, auth.checkNoEmpire, controller.post_race_name_check);

router.post('/race', auth.checkAuth, auth.checkNoEmpire, controller.post_race);

// development
router.get('/development', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, controller.get_development);

router.get('/development/upgrade', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, controller.get_development_upgrade);

// shop
router.get('/military-shop/anti-shield', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, auth.checkPopulation, controller.get_anti_shield);

router.get('/military-shop/space-fleet', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, auth.checkPopulation, controller.get_space_fleet);

router.get('/military-shop/ground-forces', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, auth.checkPopulation, controller.get_ground_forces);

router.get('/military-shop/item/:id', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, auth.checkPopulation, controller.get_military_shop_item);

router.get('/military-shop', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, auth.checkPopulation, controller.get_military_shop);

router.post('/military-shop/buy', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, auth.checkPopulation, controller.post_military_shop_buy);

// population
router.get('/population', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, controller.get_population);

router.post('/population', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, controller.post_population);

// mines
router.get('/mines/upgrade/:id', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, controller.get_mines_upgrade);

router.get('/mines', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, controller.get_mines);

// army
router.get('/army', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, controller.get_army);

router.get('/army/dismiss/:id', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, controller.get_army_dismiss); // requesting dismiss panel

router.get('/army/dismiss/:id/:amount', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, controller.get_army_dismiss_run); // the actual dismiss

// shield
router.get('/shield', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, controller.get_shield);

router.post('/shield/dome', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, controller.post_dome_action);

router.post('/shield/turrets', auth.checkAuth, auth.checkEmpire, auth.checkPlanet, auth.checkIfPlanetOwner, controller.post_turrets_action);

// profile
router.get('/profile/self', auth.checkAuth, auth.checkEmpire, controller.get_self_profile);

router.post('/profile/self', auth.checkAuth, auth.checkEmpire, controller.post_self_profile);

router.get('/profile/:id', auth.checkAuth, auth.checkEmpire, controller.get_profile);

// lore
router.get('/lore/self', auth.checkAuth, auth.checkEmpire, controller.get_self_lore);

router.post('/lore/self', auth.checkAuth, auth.checkEmpire, controller.post_self_lore);

router.get('/lore/:id', auth.checkAuth, auth.checkEmpire, controller.get_lore);

// messages
router.get('/messages/make-message/:id', auth.checkAuth, auth.checkEmpire, controller.get_make_message_with_recipient);

router.get('/messages/make-message', auth.checkAuth, auth.checkEmpire, controller.get_make_message);

router.post('/messages/make-message', auth.checkAuth, auth.checkEmpire, controller.post_make_message);

router.get('/messages/inbox', auth.checkAuth, auth.checkEmpire, controller.get_inbox);

router.get('/messages/outbox', auth.checkAuth, auth.checkEmpire, controller.get_outbox);

router.get('/messages/inbox/:id', auth.checkAuth, auth.checkEmpire, controller.get_inbox_message);

router.get('/messages/outbox/:id', auth.checkAuth, auth.checkEmpire, controller.get_outbox_message);

router.get('/messages/make-response/:id', auth.checkAuth, auth.checkEmpire, controller.get_make_response);

// scoreboards
router.get('/scoreboards', auth.checkAuth, auth.checkEmpire, controller.get_scoreboards);

// index
router.get('/index', auth.checkAuth, auth.checkEmpire, controller.get_index);

router.get('/index/classes', auth.checkAuth, auth.checkEmpire, controller.get_index_classes);

router.get('/index/resources', auth.checkAuth, auth.checkEmpire, controller.get_index_resources);

// friends
router.get('/friends/list', auth.checkAuth, auth.checkEmpire, controller.get_friend_list);

router.get('/friends/add/nickname/:name', auth.checkAuth, auth.checkEmpire, controller.get_friends_add_by_nickname);

router.get('/friends/request/:id/cancel', auth.checkAuth, auth.checkEmpire, controller.get_friend_request_cancel);

router.get('/friends/request/:id/decline', auth.checkAuth, auth.checkEmpire, controller.get_friend_request_decline);

router.get('/friends/request/:id/accept', auth.checkAuth, auth.checkEmpire, controller.get_friend_request_accept);

router.get('/friends/remove/:id', auth.checkAuth, auth.checkEmpire, controller.get_remove_friend);

// notifications
router.get('/notifications/', auth.checkAuth, auth.checkEmpire, controller.get_notifications);

// conquest
router.get('/conquest/planet-units/:id', auth.checkAuth, auth.checkEmpire, controller.get_conquest_planet_units);

router.get('/conquest/', auth.checkAuth, auth.checkEmpire, controller.get_planet_conquest);

router.post('/conquest/', auth.checkAuth, auth.checkEmpire, controller.post_planet_conquest);


module.exports = router;