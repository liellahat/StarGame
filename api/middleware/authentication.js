const db_api = require('../database/db-api');
const codes = require('../helpers/status-codes');
const errors = require('../helpers/errors');


exports.checkAuth = async function(req, res, next) {
	try {
		if (!req.session.user_id) {
			return res.status(codes.unauthorized).json({
				error: errors.authError
			});
		}

		const user = await db_api.get_user_by_id.get(req.session.user_id);

		if (!user) { 
			return res.status(codes.unauthorized).json({
				error: errors.authError
			});
		}
	
		next();

	} catch (e) {v

	}
}

exports.checkNoEmpire = async function(req, res, next) {
	try {
		const empireCheck = await db_api.check_empire_id.get(req.session.user_id);

		if (empireCheck && empireCheck.empire_id) {
			return res.status(codes.badRequest).json({
				error: errors.noEmpireError
			});
		} 

		next();

	} catch (e) {
        return errors.resError(res, e);
	}
}

exports.checkEmpire = async function(req, res, next) {
	try {
		const empireCheck = await db_api.check_empire_id.get(req.session.user_id);

		if (!empireCheck || !empireCheck.empire_id) {
			return res.status(codes.badRequest).json({
				error: errors.empireError
			});
		}

		next();

	} catch (e) {
        return errors.resError(res, e);
	}
}

exports.checkPlanet = function(req, res, next) {
	if (!req.session.planet_id) {
		return res.status(codes.notFound).json({
            error: errors.planetError
        });
	}
	
	next();
}

exports.checkIfPlanetOwner = async function(req, res, next) {
	try {
		const planet = await db_api.get_planet_owner.get(req.session.planet_id);
		
		if (planet.user_id != req.session.user_id) {
			return res.status(codes.unauthorized).json({
				error: errors.notPlanetOwnerError
			});
		}

		next();

	} catch (e) {
        return errors.resError(res, e);
	}
}

exports.checkPopulation = async function(req, res, next) {
	try {
		const planet = await db_api.get_planet_by_id_simple.get(req.session.planet_id);

		if (planet.status == 'slavery' || planet.status == 'peace') {
			return res.status(codes.badRequest).json({
				error: errors.conqueredPopulationError
			});
		}

		next();

	} catch (e) {
        return errors.resError(res, e);
	}
}

exports.checkPopulationChoiceUndecided = async function(req, res, next) {
	try {
		const status = await db_api.get_conquered_population_status.get(req.session.planet_id);

		if (status.conquered_status != 'undecided') {
			return res.status(codes.badRequest)
		}

		next();

	} catch (e) {
		return errors.resError(res, e);
	}
}