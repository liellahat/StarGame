const db_api = require('./database/db-api');
const helper = require('./helpers/helper');

exports.start = async function() {
    try {
        let users = await db_api.get_all_users.all();

        for (let i = 1; i < users.length; i++) {
            let userID = users[i].id;

            if (!users[i].empire_id) continue;

            let planets = await db_api.get_user_planets.all(userID);
        
            for (let j = 0; j < planets.length; j++) {
                let planetID = planets[j].id; 

                // adding resources
                let iron = await helper.getPlanetResourcePerHour(planetID, 'Iron');
                await helper.addResource('Iron', iron, userID);

                let hydrogen = await helper.getPlanetResourcePerHour(planetID, 'Hydrogen');
                await helper.addResource('Hydrogen', hydrogen, userID);

                let titanium = await helper.getPlanetResourcePerHour(planetID, 'Titanium');
                await helper.addResource('Titanium', titanium, userID);

                let uranium = await helper.getPlanetResourcePerHour(planetID, 'Uranium');
                await helper.addResource('Uranium', uranium, userID);

                let orium = await helper.getPlanetResourcePerHour(planetID, 'Orium');   
                await helper.addResource('Orium', orium, userID);
                
                // adding gc
                let gc = await db_api.get_planet_gc_per_hour_gain.get(planetID);
                await db_api.add_gc.run(gc.gc_payers, userID);
            }

            // reduce gcph
            let gcLoss = await db_api.get_empire_gc_per_hour_loss.get(userID);
            let gcph = gcLoss.gc_per_hour;

            // if user doesnt have enought gc
            for (let j = 0; j < planets.length && gcLoss.gc < gcph; j++) {  // iterate through user planets
                let units = await db_api.get_planet_units.all(planets[j].id);  // get planet units
                
                for (let k = 0; k < units.length; k++) {
                    let amount = units[k].amount;
                    let juba = parseInt(units[k].amount / 10);

                    // reduce a 1/10 of the amount until the amount is 0 or the user has enough gc to pay.
                    while (gcLoss.gc < gcph && amount > juba) {
                        await helper.dismissUnit(units[k].id, planets[j].id, juba, userID);

                        // reduce amount and gc
                        amount -= juba;
                        gcph -= juba * units[k].unit_gcph;
                    }
                }
            }
        }
        
        // population growth
        let populations = await db_api.get_all_populations_growth_data.all();

        for (let i = 0; i < populations.length; i++) {
            let populationData = populations[i];

            let growthFactor = 1.1 + populationData.breed_rate / 30;

            // changing state
            let size = populationData.size + populationData.ready_for_combat + populationData.ready_for_combat_support;

            let ratio = size / populationData.capacity;

            let state = populationData.state;

            if (ratio > 1) {
                state -= ratio * 0.5;
            } else if (ratio < 1) {
                state += ratio;
            }

            if (state < 0) {
                state = 0;
            } else if (state > 100) {
                state = 100;
            }

            const newSize = parseInt(populationData.size * growthFactor);
            const newCombatReady = parseInt(populationData.ready_for_combat * growthFactor);
            const newCombatSuppReady = parseInt(populationData.ready_for_combat_support * growthFactor);
            const newGcPayers = newSize - populationData.mine_1_workers - populationData.mine_2_workers;

            await db_api.breed.run(newSize, newCombatReady, newCombatSuppReady, parseInt(state), newGcPayers, populationData.id);
        }        

    } catch (e) {
        console.log('error', e);
    }
}