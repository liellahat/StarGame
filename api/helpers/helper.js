const db_api = require('../database/db-api');
const errors = require('./errors');


exports.sleep = function(ms) {
    return new Promise(function(resolve) {
        setTimeout(resolve, ms);
    });
}

exports.dismissUnit = async function(unitID, planetID, amount, userID) {    
    return new Promise(async function(resolve, reject) {
        try {
            // this query also checks if the unit belongs to the planet army.
            const unit = await db_api.get_army_unit_by_id.get(unitID, planetID);
            if (!unit) {
                reject('Unit doesn\'t exist.');
            }
        
            if (unit.amount < amount) {
                reject('Don\'t hack us please.');
            }
        
            // decreasing combat and combat support        
            const combat = amount * unit.combat;
            const combatSupport = amount * unit.combat_support;
            
            await db_api.dismiss_people.run(combat, combatSupport, planetID);
            
            // decrease gcph
            const gcph = amount * unit.gc_per_hour;
        
            await db_api.add_gc_per_hour.run(-1 * gcph, userID);
        
            // adding combat and combat support population
            await db_api.add_dismissed_people.run(combat, combatSupport, planetID);
        
            // decreasing/deleting units
            if (unit.amount == amount) {
                await db_api.delete_army_unit.run(unitID);
            } else {
                await db_api.reduce_army_units.run(amount, unitID);
            }  

        } catch (e) {
            reject(e);
        }

        resolve();
    });
}

exports.isEmail = function(email) {
    // from https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return re.test(String(email).toLowerCase());
}

exports.delayedTime = function(delay) {
    let time = new Date();
    time.setSeconds(time.getSeconds() + delay);

    return time;
}


// gets
exports.getPlanetsRank = async function(userID) {
    return new Promise(async function(resolve, reject) {
        try {
            const scoreboard =  await db_api.get_planets_scoreboard.all();
            
            for (let i = 0; i < scoreboard.length; i++) {
                if (scoreboard[i].user_id == userID) {
                    resolve(i + 1);
                }
            }

            resolve(null);

        } catch (e) {
            reject(e);
        }
    });    
}

exports.getMilitaryRank = async function(userID) {
    return new Promise(async function(resolve, reject) {
        try {
            const scoreboard =  await db_api.get_military_power_scoreboard.all();
        
            for (let i = 0; i < scoreboard.length; i++) {
                if (scoreboard[i].user_id == userID) {
                    resolve(i + 1);
                }
            }

            resolve(null);

        } catch (e) {
            reject(e);
        }
    });
}

exports.getPlanetsScoreboard = function() {
    return new Promise(async function(resolve, reject) {
        try {
            const scoreboard =  await db_api.get_planets_scoreboard.all();
            const result = [];

            for (let i = 0; i < 10; i++) {
                result.push({ rank: i + 1, item: scoreboard[i] });
            }

            resolve(result);

        } catch (e) {
            reject(e);
        }
    }); 
}

exports.getMilitaryScoreboard = function() {
    return new Promise(async function(resolve, reject) {
        try {
            const scoreboard =  await db_api.get_military_power_scoreboard.all();
            const result = [];

            for (let i = 0; i < 10; i++) {
                result.push({ rank: i + 1, item: scoreboard[i] });
            }

            resolve(result);

        } catch (e) {
            reject(e);
        }
    }); 
}

exports.getPlanetResourcePerHour = async function(id, resourceName) {
    return new Promise(async function(resolve, reject) {
        try {                    
            let r1 = await db_api.get_planet_resource_per_hour_1.get(id, resourceName);
            let r2 = await db_api.get_planet_resource_per_hour_2.get(id, resourceName);
            let sum = 0;

            if (r1) sum += r1.per_hour;
            if (r2) sum += r2.per_hour;

            resolve(sum);
        } catch (e) {
            reject(e);
        }
    });
}

exports.getPlanetsDistance = function(id1, id2) {
    return new Promise(function(resolve, reject) {
        try {
            resolve(50);
        } catch (e) {
            reject(e);
        }
    });
}


// updates
exports.updatePlanetArmy = function(planetID) {
    return new Promise(async function(resolve, reject) {
        try {
            const userID = (await db_api.get_planet_owner.get(planetID)).user_id;
            const initialMilitaryPower = (await db_api.get_planet_military_power.get(planetID)).military_power;

            let readyRows = await db_api.get_planet_ready_waiting_units.all(planetID);  // ready units in the waiting list.
            let planetRows = await db_api.get_planet_units.all(planetID);  // already ready units.

            // this is an O(n^2) solution. we could do an O(nlogn) solution but we are talking about small n values so it doesn't really matter.
            for (let i = 0; i < readyRows.length; i++) {
                let j;
                for (j = 0; j < planetRows.length; j++) {
                    if (planetRows[j].unit_type_id == readyRows[i].unit_type_id) {
                        await db_api.add_ready_units.run(readyRows[i].amount, readyRows[i].unit_type_id, readyRows[i].army_id);

                        // buying gc per hour
                        await db_api.add_gc_per_hour.run(readyRows[i].amount * readyRows[i].unit_gcph, userID);

                        break;
                    }
                }
                
                // if its a new unit
                if (j == planetRows.length) {
                    planetRows.push(readyRows[i]);
                    await db_api.add_new_ready_units.run(readyRows[i].unit_type_id, readyRows[i].army_id, readyRows[i].amount);

                    await db_api.add_gc_per_hour.run(readyRows[i].amount * readyRows[i].unit_gcph, userID);
                }
                
                await db_api.clean_ready_units.run(planetID);
            }

            // update planet military power
            const armyMilitaryPower = await db_api.get_planet_army_military_power.get(planetID);
            const shield = await db_api.get_planet_shield.get(planetID);
        
            const militaryPower = armyMilitaryPower.army_military_power + shield.shield_military_power;

            await db_api.update_planet_military_power.run(militaryPower, planetID);

            // update empire military power
            const militaryPowerDifference = militaryPower - initialMilitaryPower;
            
            await db_api.update_empire_military_power.run(militaryPowerDifference, userID);

            resolve();

        } catch (e) {
            reject(e);
        }
    });
}

exports.updateUserPlanetArmies = async function(userID) {
    const userPlanets = await db_api.get_user_planets.all(userID);

    for (let i = 0; i < userPlanets.length; i++) {
        await exports.updatePlanetArmy(userPlanets[i].id);
    }    
}


// user functionalities
exports.login = async function(req, userID) {
    const empireCheck = await db_api.check_empire_id.get(userID);  // check if the user has an empire.

    if (empireCheck.empire_id) {
        const loginData = await db_api.get_login_data_with_empire.get(userID);
        
        req.session.user_id = userID;
        req.session.username = loginData.username;
        req.session.nickname = loginData.nickname;
        req.session.empire_name = loginData.empire_name;
        req.session.race_name = loginData.race_name;
    } else {
        const loginData = await db_api.get_login_data_without_empire.get(userID);
        
        req.session.user_id = userID;
        req.session.username = loginData.username;
        req.session.nickname = loginData.nickname;
        req.session.empire_name = null;
        req.session.race_name = null;
    }
}

exports.logout = function(req) {
    req.session.user_id = null;
    req.session.username = null;
    req.session.nickname = null;
    req.session.empire_name = null;
    req.session.race_name = null;
    req.session.planet_id = null;
    req.session.planet_name = null;
    req.session.visited_planet_id = null;
    req.session.visited_planet_name = null;
}


// resource transactions
exports.addResource = function(resourceName, amount, user_id) {    
    resourceName = resourceName.toLowerCase();

    return new Promise(async function(resolve, reject) {
        try {
            if (resourceName == 'iron') await db_api.add_iron.run(amount, user_id);
            else if (resourceName == 'titanium') await db_api.add_titanium.run(amount, user_id);
            else if (resourceName == 'hydrogen') await db_api.add_hydrogen.run(amount, user_id);
            else if (resourceName == 'uranium') await db_api.add_uranium.run(amount, user_id);
            else if (resourceName == 'orium') await db_api.add_orium.run(amount, user_id);
            else if (resourceName == 'gc') await db_api.add_gc.run(amount, user_id);
        } catch (e) {
            reject(e);
        }
        
        resolve();
    });
}

exports.addTransfer = function(resourceName, amount, user_id, delay) {
    return new Promise(async function(resolve, reject) {
        try {
            let time = new Date();
            time.setSeconds(time.getSeconds() + delay);
        
            await db_api.add_transfer.run(resourceName, amount, user_id, delay);

            resolve();
        } catch (e) {
            reject(e);
        }
    });
}

exports.makeUserTransfers = function(userID) {
    return new Promise(async function(resolve, reject) {
        try {
            const transfers = await db_api.get_user_transfers.all(userID);
        
            for (let i = 0; i < transfers.length; i++) {
                exports.addResource(transfers[i].resource_name, transfers[i].amount, userID);
            }
        
            await db_api.delete_old_user_transfers.run(userID);

            resolve();
        } catch (e) {
            reject(e);
        }
    })
}


// random events
exports.eventOccurrence = function(percentage) {
    const rnd = Math.floor(Math.random() * 100);  // between 0 and 99

    return percentage > rnd; 
}

exports.multipleEventOccurrence = function() {
    const rnd = Math.floor(Math.random() * 100);  // between 0 and 99
    let sum = 0;

    for (let i = 0; i < arguments.length; i++) {
        sum += arguments[i];

        if (sum > rnd) return i;
    }

    return arguments.indexOf(Math.max(...arguments));;
}

exports.RNG = function(a, b) {
    return Math.floor(Math.random() * b) + a;
}


// formats
exports.formatDate = function(date) {
    options = {
        year: '2-digit',
        month:'2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit'
    };

    return '20' + date.toLocaleString(undefined, options);  // no easy way to use 4 digit year.
}

exports.formatNumber = function(num) {         
    if (typeof(num) == 'string') {
        num = parseInt(num);
    }

    if (num > 10000000000000) return Math.round(num / 1000000000000 * 10) / 10 + 'Q';
    if (num > 10000000000) return Math.round(num /1000000000 * 10) / 10 + 'B';
    if (num > 10000000) return Math.round(num / 1000000 * 10) / 10 + 'M'; 
    if (num > 10000) return Math.round(num / 1000 * 10) / 10 + 'K';

    return Math.round(num * 10) / 10;
}

exports.capitalize = function(str) {
    return str[0].toUpperCase() + str.slice(1);
}


// those function throw an error (that is given as a parameter) if a condition is met.
exports.nonIntFields = function() {
    const body = arguments[0];
    const err = arguments[arguments.length - 1];

    for (let i = 1; i < arguments.length - 1; i++) {
        if (body[arguments[i]] != parseInt(body[arguments[i]])) throw err;
    }
}

exports.negativeFields = function() {
    const body = arguments[0];
    const err = arguments[arguments.length - 1];

    for (let i = 0; i < arguments.length; i++) {
        if (body[arguments[i]] < 0) throw err;
    }
}

exports.nonPositiveFields = function() {
    const body = arguments[0];
    const err = arguments[arguments.length - 1];

    for (let i = 0; i < arguments.length; i++) {
        if (body[arguments[i]] <= 0) throw err;
    }
}

exports.outOfLimits = function() {
    const body = arguments[0];
    const min = arguments[arguments.length - 3];
    const max = arguments[arguments.length - 2];
    const err = arguments[arguments.length - 1];

    for (let i = 1; i < arguments.length - 3; i++) {
        str = arguments[i];

        if (str.length > max || str.length < min) throw err;
    }
}