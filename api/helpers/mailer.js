const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');

let transporter;

exports.setup = function (email, userGuid) {
    transporter = nodemailer.createTransport(smtpTransport({
        service: 'gmail',
        host: 'smtp.gmail.com',
        auth: {
            user: process.env.EMAIL,
            pass: process.env.EMAIL_PASSWORD
        }})
    );
}

//exports.setup()
exports.send = function(email, subject, text) {
    return new Promise(function(resolve, reject) {
        mailOptions = {
           from: process.env.EMAIL,
           to: email,
           subject: subject,
           text: text
       };
   
       transporter.sendMail(mailOptions, async function(err, info) {
           if (err) {
               reject(err);
           } else {
               resolve();
           }
       });
    });
}