const codes = require('./status-codes');

exports.Error = function(code, msg) {
    this.code = code;
    this.msg = msg;
}

exports.resError = function(res, e) {    
    console.log(e);
    
    if (e instanceof exports.Error) {
        return res.status(e.code).json({
            error: e.msg
        })
    }

    return res.status(codes.badRequest).json({
        error: exports.error
    });
}

exports.sessionError = function(req, res, e, nextPage) {
    console.log(e);

    if (e instanceof exports.Error) {
        req.session.error = e.msg
    } else {
        req.session.error = exports.error
    }

    return res.redirect(nextPage);
}

exports.butWhyError = 'But why?';

exports.sqlError = 'SQL error. Try again later.';

exports.authError = 'You must be logged in to view this page.';

exports.noEmpireError = 'You can\'t view this page because you already have an empire.';

exports.empireError = 'You can\'t view this page because you don\'t have an empire.';

exports.planetError = 'You can\'t view this page because you are not on a planet.';

exports.renderError = 'Could not render the view.';

exports.viewError = 'Could not read the view file.';

exports.hashError = 'Hash error. Try again later.';

exports.nanError = 'Value is not a number.';

exports.notPlanetOwnerError = 'This planet isn\'t yours.';

exports.invalidActionError = 'Invalid action.';

exports.internalCaptchaError = 'Internal captcha verification error. Try again.';

exports.userCaptchaError = 'Captcha verificaton failed. Are you a human?';

exports.raceNameTakenError = 'This race name is taken.';

exports.empireNameTakenError = 'This empire name is taken.';

exports.insufficientResourceError = 'Insufficient resource.';

exports.insufficientGcError = 'Insufficient Galactic Currency.';

exports.insufficientIronError = 'Insufficient Iron.';

exports.insufficientHydrogenError = 'Insufficient Hydrogen.';

exports.insufficientTitaniumError = 'Insufficient Titanium.';

exports.insufficientUraniumError = 'Insufficient Uranium.';

exports.insufficientOriumError = 'Insufficient Orium.';

exports.insufficientCombatReadyError = 'Insufficient combat ready population.';

exports.insufficientSupportReadyError = 'Insufficient combat support ready population.'

exports.insufficientCombatAndSupportReadyError = 'Insufficient combat and combat support ready population.';

exports.planetNotFoundError = 'Planet not found.';

exports.missingFieldError = 'A field is missing in your request.';

exports.nonIntegerError = 'A value is not an integer.';

exports.negativeError = 'A value is negative.'

exports.missingUsernameError = 'Please enter a username.';

exports.missingNicknameError = 'Please enter a nickname.';

exports.missingEmailError = 'Please enter an email.';

exports.missingPasswordError = 'Please enter a password.';

exports.missingPasswordConfirmError = 'Please confirm your password.';

exports.usernameTakenError = 'This username is taken.';

exports.nicknameTakenError = 'This nickname is taken.';

exports.emailTakenError = 'This email address is used by another user.';

exports.loginError = 'Username and password do not match.';

exports.missingRaceNameError = 'Please choose a name for your race.';

exports.invalidRaceNameLengthError = 'Race name has to contain 3-16 characters.';

exports.shopZeroUnitsError = 'Please choose more than 0 units.';

exports.mineMaxLevelError = 'You must develop your planet to upgrade this mine.';

exports.userNotFoundError = 'User not found.';

exports.missingEmpireNameError = 'Please choose a name for your empire.';

exports.invalidEmpireNameLengthError = 'Empire name has to contain 3-16 characters.';

exports.invalidNicknameLengthError = 'Nickname must contain 3-16 characters.';

exports.recipientEmptyError = 'Please enter the recipient name.';

exports.topicEmptyError = 'Please enter the topic of the message.';

exports.messageEmptyError = 'Please enter a message.';

exports.invalidNicknameError = 'Nickname is invalid.';

exports.invalidPasswordError = 'Password must contain at least 4 characters.';

exports.passwordConfirmError = 'Password does not match password confirmation.';

exports.invalidEmailError = 'Invalid email address.';

exports.invalidTopicLengthError = 'Topic length must be between 1 and 45 characters.';

exports.invalidUsernameLengthError = 'Username must contain 3-16 characters.';

exports.messageAuthError = 'You are not authorized to view this message.';

exports.requestExists = 'Request exists.';

exports.alreadyFriends = 'You are already friends.';

exports.planetHasArmyError = 'The planet is protected by an army. You must attack it before you conquer it.';

exports.homePlanetConquestError = 'This planet is a home planet of another empire, you cannot attack it.';

exports.conquestZeroUnitsError = 'You must deploy at least one unit.';

exports.conqueredPopulationError = 'You can\'t do that, the population is conquered.';

exports.error = 'Error.';

exports.invalidValueError = 'Invalid value.';