const helper = require('./helper');

// development
exports.developmentUpgradeGcCost = function(level) {
    if (level == 0) {
        return 1000;
    }

    return 500 * 1.3 ** (level - 1);
}

exports.developmentUpgradeIronCost = function(level) {
    if (level == 0) {
        return 1000;
    }

    return 400 * 1.3 ** (level - 1);
}

exports.developmentUpgradeTime = function(level) {
    if (level == 0) {
        return 5000;
    }
    
    return 3600 * 1.2 ** (level - 1);
}

exports.minesMaxLevel = function(level) {
    return level * 4 + 4;
}

exports.minesNextMaxLevel = function(level) {
    return level * 4 + 8;
}

exports.planetCapacity = function(level) {
    return level * 200000000 + 200000000;
}

exports.planetNextCapacity = function(level) {
    return level * 200000000 + 400000000;
}


// mines
exports.mineUpgradeCost = function(level) {
    return 100 * 1.2 ** (level - 1);
}

exports.mineUpgradeTime = function(level) {
    return 30 * 1.2 ** (level - 1);
}

exports.minePerHour = function(level) {
    return 200 * 1.2 ** (level - 1);
}


// shield
exports.domeUpgradeCost = function(level) {
    if (level == 0) {
        return 800;
    }

    return 300 * 1.3 ** (level - 1);
}

exports.domeRepairCost = function(level) {
    return 180 * 1.25 ** (level - 1); ;
}

exports.domeUpgradeTime = function(level) {
    if (level == 0) {
        return 300;
    }

    return 80 * 1.2 ** (level - 1);
}

exports.domeRepairTime = function(level) {
    return 60 * 1.2 ** (level - 1); 
}

exports.turretsUpgradeCost = function(level) {
    if (level == 0) {
        return 700;
    }

    return 250 * 1.35 ** (level - 1);  
}

exports.turretsRepairCost = function(level) {
    return 150 * 1.1 ** (level - 1);
}

exports.turretsUpgradeTime = function(level) {
    if (level == 0) {
        return 250;
    }

    return 80 * 1.2 ** (level - 1);
}

exports.turretsRepairTime = function(level) {
    return 60 * 1.2 ** (level - 1);
}

exports.domeDefense = function(level) {
    if (level == 0) {
        return 0;
    }

    return 30 * 1.3 ** (level - 1);
}

exports.turretsAttack = function(level) {
    if (level == 0) {
        return 0;
    }

    return 20 * 1.3 ** (level - 1);
}


// conquest
exports.resourceLootAmount = function(level) {
    return helper.RNG(500, 5000) * (level + 1); 
}

exports.gcLootAmount = function(level) {
    return helper.RNG(1000, 10000) * (level + 1); 
}

exports.conquestDelay = function(distance) {
    return distance * 2000; // + helper.RNG(2000, 6000);
}


// population choice
exports.exterminationDelay = function(populationSize) {
    return populationSize * 0.01;
}