const db_api = require('../database/db-api');
const helper = require('./helper');


function add(userID, delay, text) {
    return new Promise(async function(resolve, reject) {
        try {
            let revealTime = new Date();
            revealTime.setSeconds(revealTime.getSeconds() + delay);
            
            await db_api.add_notification.run(userID, helper.formatDate(revealTime), text);  
            
            resolve();

        } catch (e) {
            reject(e);
        }
    });
}

// race
exports.welcome = async function(userID, delay) {
    const msg =  'WELCOME TO ELION!';
    await add(userID, delay, msg);
} 

// development
exports.developmentUpgrade = async function(userID, delay, planetName) {
    const msg =  `You have developed your planet: ${planetName}.`;
    await add(userID, delay, msg);
}

// mines
exports.mineUpgrade = async function(userID, delay, planetName, resource) {
    const msg = `You have upgraded the ${resource} mine in your planet: ${planetName}.`;
    await add(userID, delay, msg);
}

// shield
exports.domeUpgrade = async function(userID, delay, planetName) {
    const msg = `You have upgraded the shield dome in your planet: ${planetName}.`;
    await add(userID, delay, msg);
}

exports.domeRepair = async function(userID, delay, planetName) {
    const msg = `You have repaired the shield dome in your planet: ${planetName}.`;
    await add(userID, delay, msg);
}

exports.turretsUpgrade = async function(userID, delay, planetName) {
    const msg = `You have upgraded the shield turrets in your planet: ${planetName}.`;
    await add(userID, delay, msg);
}

exports.turretsRepair = async function(userID, delay, planetName) {
    const msg = `You have repaired the shield turrets in your planet: ${planetName}.`;
    await add(userID, delay, msg);
}

// conquest
exports.planetConqueredAttacker = async function(userID, delay, planetName) {  // (no defender)
    const msg = `You have succesfully conquered the planet: ${planetName}. You can now populate it if you wish to.`;    
    await add(userID, delay, msg);
}

exports.planetConqueredAndLootedAttacker = async function(userID, delay, planetName, resource1, resource2, amount1, amount2, gcAmount) {
    const msg = `You have successfully conquered and looted the planet: ${planetName}. You have earned:
        <div class="notification-line-resources">
        <img src="/images/icons/${resource1}.png"> x ${helper.formatNumber(amount1)}<br/>
        <img src="/images/icons/${resource2}.png"> x ${helper.formatNumber(amount2)}<br/>
        <img src="/images/icons/gc.png"> x ${helper.formatNumber(gcAmount)}<br/>
        </div>
        You can now decide the fate of the population.`
        .replace(/    /g, '');
    
    console.log(msg);
    await add(userID, delay, msg);
}

exports.planetConqueredAndLootedDefender = async function(userID, delay, planetName, attackerName, attackerEmpireName, resource1, resource2, amount1, amount2, gcAmount) {
    const msg = `${helper.capitalize(attackerName)}, the emperor of ${attackerEmpireName} has conquered and looted your planet: ${planetName}. You have lost:
        <div class="notification-line-resources">
        <img src="/images/icons/${resource1}.png"> x ${helper.formatNumber(amount1)}<br/>
        <img src="/images/icons/${resource2}.png"> x ${helper.formatNumber(amount2)}<br/>
        <img src="/images/icons/gc.png"> x ${helper.formatNumber(gcAmount)}<br/>
        </div>
        The fate of the planet's population is in the hands of the conquerer.`
        .replace(/    /g, '');
    
    await add(userID, delay, msg);
}