exports.ok = 200;

exports.badRequest = 400;

exports.unauthorized = 401;

exports.forbidden = 403;

exports.notFound = 404;

exports.serverError = 500;