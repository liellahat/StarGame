const sqlite3 = require('sqlite3');

const db = new sqlite3.Database('./database.db');

let prepareSQL = function(sql) {
    return {
        get: function() {
            const args = Object.values(arguments);            

            return new Promise(function(resolve, reject) {                
                for (let i = 0; i < args.length; i++) {
                    if (args == undefined) reject(new Error('One of the arguments is undefined.')); 
                }

                db.get(sql, args, function(err, data) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
            });            
        },
        all: function() {
            const args = Object.values(arguments);

            return new Promise(function(resolve, reject) {                
                for (let i = 0; i < args.length; i++) {
                    if (args == undefined) reject(new Error('One of the arguments is undefined.')); 
                }

                db.all(sql, args, function(err, rows) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(rows);
                    }
                });
            });
        },
        run: function() {
            const args = Object.values(arguments);

            return new Promise(function(resolve, reject) {                
                for (let i = 0; i < args.length; i++) {
                    if (args == undefined) reject(new Error('One of the arguments is undefined.')); 
                }

                db.run(sql, args, function(err) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            });
        }
    };
}

exports.build_db = function() {
    try {
        db.serialize(function() {      
            db.run('create table if not exists users(\
                id integer primary key autoincrement,\
                username text,\
                nickname text,\
                password_hash text,\
                empire_id integer,\
                email text,\
                guid text,\
                verified integer,\
                message_timeout datetime,\
                foreign key(empire_id) references empires(id));');            
    
            db.run('create table if not exists empires(\
                id integer primary key autoincrement,\
                name text,\
                race_id integer,\
                gc integer,\
                iron integer,\
                titanium integer,\
                uranium integer,\
                hydrogen integer,\
                orium integer,\
                gc_per_hour integer,\
                home_planet_id integer,\
                planets integer,\
                military_power real,\
                honor integer,\
                foreign key(race_id) references races(id),\
                foreign key(home_planet_id) references planets(id));');            
    
            db.run('create table if not exists races(\
                id integer primary key autoincrement,\
                name text,\
                lore text,\
                survivability integer,\
                breed_rate integer,\
                intellegence integer,\
                combat_instincts integer);');           
    
            db.run('create table if not exists resources(\
                id integer primary key autoincrement,\
                name text,\
                pic_path text,\
                description text);');            
    
            db.run('create table if not exists planets(\
                id integer primary key autoincrement,\
                name text,\
                capacity integer,\
                empire_id integer,\
                population_id integer,\
                mines_id integer,\
                survivability_requirement integer,\
                texture text,\
                army_id integer,\
                shield_id integer,\
                development_level integer,\
                development_wait_till datetime,\
                military_power real,\
                solar_system_id integer,\
                orbit integer,\
                military_power_requirement real,\
                conquest_time datetime,\
                last_empire_id integer,\
                estimated_military_power real,\
                foreign key(empire_id) references empires(id),\
                foreign key(population_id) references populations(id),\
                foreign key(mines_id) references mines(id),\
                foreign key(army_id) references armies(id),\
                foreign key(shield_id) references shields(id),\
                foreign key(solar_system_id) references solar_systems(id),\
                foreign key(last_empire_id) references empires(id));');            
    
            db.run('create table if not exists populations(\
                id integer primary key autoincrement,\
                race_id integer,\
                size integer,\
                ready_for_combat integer,\
                ready_for_combat_support integer,\
                mine_1_workers integer,\
                mine_2_workers integer,\
                gc_payers integer,\
                state integer,\
                conquered_status text,\
                extermination_time datetime,\
                foreign key(race_id) references races(id));');              
    
            db.run('create table if not exists mines(\
                id integer primary key autoincrement,\
                resource_1 text,\
                resource_2 text,\
                level_1 integer,\
                level_2 integer,\
                max_level integer,\
                per_hour_1 integer,\
                per_hour_2 integer,\
                wait_till_1 datetime,\
                wait_till_2 datetime,\
                foreign key(resource_1) references resources(name),\
                foreign key(resource_2) references resources(name));');          
    
            db.run('create table if not exists unit_types(\
                id integer primary key autoincrement,\
                name text,\
                class_name integer,\
                attack real,\
                defense real,\
                gc_per_hour integer,\
                resource_1 text,\
                resource_1_amount integer,\
                resource_2 text,\
                resource_2_amount integer,\
                combat integer,\
                combat_support integer,\
                pic_path text,\
                juba integer,\
                wait_duration real,\
                foreign key(class_name) references classes(name),\
                foreign key(resource_1) references resources(name),\
                foreign key(resource_2) references resources(name));');            
    
            db.run('create table if not exists classes(\
                id integer primary key autoincrement,\
                name text,\
                description text,\
                type text,\
                countered_class_name text,\
                foreign key(countered_class_name) references classes(name));');            
    
            db.run('create table if not exists solar_systems(\
                id integer primary key autoincrement,\
                x integer,\
                y integer);');            
    
            db.run('create table if not exists armies(\
                id integer primary key autoincrement,\
                combat_support_size integer,\
                combat_size integer,\
                target_planet integer,\
                original_army_id integer,\
                foreign key(target_planet) references planets(id),\
                foreign key(original_army_id) references armies(id));');                    
    
            db.run('create table if not exists units_in_armies(\
                id integer primary key autoincrement,\
                army_id integer,\
                unit_type_id integer,\
                amount integer,\
                foreign key(army_id) references armies(id),\
                foreign key(unit_type_id) references unit_types(id));');            
    
            db.run('create table if not exists units_wait(\
                id integer primary key autoincrement,\
                army_id integer,\
                unit_type_id integer,\
                amount integer,\
                ready_time datetime,\
                foreign key(army_id) references armies(id),\
                foreign key(unit_type_id) references unit_types(id));');

            db.run('create table if not exists shields(\
                id integer primary key autoincrement,\
                dome_level integer,\
                dome_defense integer,\
                dome_ruined integer,\
                dome_wait_till datetime,\
                turrets_level integer,\
                turrets_attack integer,\
                turrets_ruined integer,\
                turrets_wait_till datetime);');
    
            db.run('create table if not exists battles(\
                id integer primary key autoincrement,\
                attacker_id integer,\
                defender_id integer,\
                type text,\
                winner text,\
                gc_loot integer,\
                resource_1 text,\
                resource_1_loot,\
                resource_2 text,\
                resource_2_loot,\
                foreign key(attacker_id) references armies(id),\
                foreign key(defender_id) references armies(id),\
                foreign key(resource_1) references resources(name),\
                foreign key(resource_2) references resources(name));');
                
            db.run('create table if not exists losses(\
                id integer primary key autoincrement,\
                battle_id integer,\
                side text,\
                unit_type_id integer,\
                amount integer,\
                foreign key(battle_id) references battles(id),\
                foreign key(unit_type_id) references unit_types(id));');            
    
            db.run('create table if not exists messages(\
                id integer primary key autoincrement,\
                sender_id integer,\
                receiver_id integer,\
                content text,\
                topic text,\
                time datetime,\
                read integer,\
                foreign key(sender_id) references users(id),\
                foreign key(receiver_id) references users(id));');

            db.run('create table if not exists friends(\
                id integer primary key autoincrement,\
                user1_id integer,\
                user2_id integer,\
                accepted integer,\
                foreign key(user1_id) references users(id),\
                foreign key(user2_id) references users(id));');
        
            db.run('create table if not exists notifications(\
                id integer primary key autoincrement,\
                user_id integer,\
                reveal_time datetime,\
                notification text,\
                viewed integer,\
                foreign key(user_id) references users(id));');

            db.run('create table if not exists transfers(\
                id integer primary key autoincrement,\
                resource_name text,\
                amount integer,\
                user_id integer,\
                time datetime,\
                foreign key(user_id) references users(id));');
    
            db.all('select * from resources;', [], function(err, rows) {
                if (err) {
                    throw err;
                }
    
                if (rows.length == 0) {
                    db.run('insert into resources(name, description, pic_path)\
                        values("Iron", "The most basic crafting material. Used for building spacecrafts and structures.", "/images/icons/iron.png"),\
                        ("Titanium", "Used for building stronger spacecrafts and structures", "/images/icons/iron.png"),\
                        ("Hydrogen", "Common type of fuel that fits most spacecrafts. Also used as ammo for light weapons.", "/images/icons/iron.png"),\
                        ("Uranium", "Used as ammo for heavy weapons and fuel for large spacecrafts.", "/images/icons/iron.png"),\
                        ("Orium", "Used for powering planet shields and spacecraft domes.", "/images/icons/iron.png"),\
                        ("Galactic Currency", "Galactic Currency is a currency unit agreed upon throughout the galaxy.", "/images/icons/gc.png");');
                }
            });
    
            db.all('select * from classes;', [], function(err, rows) {
                if (err) {
                    throw err;
                }
    
                if (rows.length == 0) {
                    db.run('insert into classes(name, description, type, countered_class_name)\
                        values("Fighter Ship", "Light and agile battleships armed with light cannons.", "Space Fleet", "Space Commando"),\
                        ("Heavy Cannon", "Heavily weaponed battleships, armed with powerful long range cannons.", "Space Fleet", "Fighter Ship"),\
                        ("Dome Deployer", "A defensive spaceship capble of deploying shields that raise the total defense power of the space fleet.", "Space Fleet", "Heavy Cannon"),\
                        ("Space Commando", "Groups of highly trained individuals who penetrate the enemy lines and sabotage their defenses.", "Space Fleet", "Dome Deployer"),\
                        ("Tank", "Large and heavily armored units designed for frontline combat, operated by multiple soldiers.", "Ground Forces", "Combat Unit"),\
                        ("Air Bomber", "Combat aircrafts designed to attack ground targets by dropping bombs and deploying missiles.", "Ground Forces", "Tank"),\
                        ("Fighter Jet", "Highly maneuverable aircrafts designed primarily for aerial combat.", "Ground Forces", "Air Bomber"),\
                        ("Combat Unit", "Large infantry divisions composed by several regiments, aimed at conquering and taking down targets and enemy weapons.", "Ground Forces", "Artillery"),\
                        ("Artillery", "Large and poweful weapons, capable of firing ammunition or launching missiles and take down far enemy targets and aircrafts.", "Ground Forces", "Air Bomber"),\
                        ("Anti Shield Cannon", "Heavy battleships that carry weapons designed for breaking through planet shield domes.", "Anti Shield", "Shield Dome"),\
                        ("Anti Shield Dome", "Defensive spacecrafts that deploy shields designed specifically at blocking turret fire, in order to protect your Anti Shield Cannons", "Anti Shield", "Shield Turrets");');
                
                    // average: atk: 0.001,    def: 0.0003,    gc_per_hour: 37.5,    resource_1:               
                    db.run('insert into unit_types(name, class_name, attack, defense, gc_per_hour, resource_1, resource_1_amount, resource_2, resource_2_amount, combat, combat_support, pic_path, juba, wait_duration)\
                        values("Zen Fighter", "Fighter Ship", 0.001, 0.0003, 0.01, "Iron", 0.1, "Hydrogen", 0.1, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Armored Taj", "Fighter Ship", 0.009, 0.00045, 0.012, "Titanium", 0.13, "Hydrogen", 0.1, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5),\
                        ("Damian Destroyer", "Fighter Ship", 0.0014, 0.0025, 0.012, "Iron", 0.15, "Hydrogen", 0.15, 20, 2000, "/images/units/fighter-ship.jpg", 5000, 0.5);');
                }
    
                db.all('select * from planets;', function(err, data) {
                    if (err) {
                        throw err;
                    }
    
                    if (data.length == 0) {
                        // 1 stands for no empire, no race
                        // 2 stands for local races with no empires
                        db.run('insert into users(username, nickname, password_hash, empire_id, email, verified)\
                            values(null, null, null, 1, null, null),\
                            (null, null, null, 2, null, null);');

                        db.run('insert into races(name, lore, survivability, breed_rate, intellegence, combat_instincts)\
                            values(null, null, null, null, null, null),\
                            (null, null, null, null, null, null);');

                        db.run('insert into empires(name, race_id, gc, iron, titanium, uranium, hydrogen, orium, gc_per_hour, home_planet_id, planets, military_power)\
                            values(null, null, 1, null, null, null, null, null, null, null, null, null),\
                            (null, null, 2, null, null, null, null, null, null, null, null, null);');
                        
                        let k = 1;
                        for (let i = 1; i <= 5; i++) {
                            db.run('insert into solar_systems(x, y) values(100, 100);');

                            for (let j = 1; j <= 6; j++, k++) {       
                                db.serialize(function() {
                                    const id = (Math.round(Math.random()) + 1);
                                    let populationSize = 0;

                                    if (id == 2) {
                                        populationSize = 10000000000; 
                                    }

                                    db.run(`insert into planets(name, development_level, development_wait_till, capacity, empire_id, population_id, mines_id,\
                                        army_id, shield_id, survivability_requirement, military_power, texture, solar_system_id, orbit, military_power_requirement, conquest_time, estimated_military_power)\
                                        values("Planet ${k}", 0, null, 200000000000, ${id}, ${k}, ${k}, ${k}, ${k}, 0, 0, "/images/textures/texture.png", ${i}, ${j}, 0.01, null, 0);`); 
                                    
                                    db.run(`insert into populations(race_id, size, ready_for_combat, ready_for_combat_support, mine_1_workers, mine_2_workers, gc_payers, state, extermination_time, conquered_status)\
                                        values(${id}, ${populationSize}, ${populationSize / 1000}, ${populationSize / 100}, 0, 0, ${populationSize}, ${populationSize != 0 ? 100 : null}, null, null);`);  
        
                                    db.run('insert into mines(resource_1, resource_2, level_1, level_2, max_level, per_hour_1, per_hour_2, wait_till_1, wait_till_2)\
                                        values("Iron", "Hydrogen", 0, 0, 4, 0, 0, null, null);');
        
                                    db.run('insert into armies(combat_support_size, combat_size)\
                                        values(0, 0);');
        
                                    db.run('insert into shields(dome_level, dome_defense, dome_ruined, turrets_level, turrets_attack, turrets_ruined)\
                                        values(0, 0, 0, 0, 0, 0);');
                                });
                            }
                        }
                    }
                });
            });   
    
            let populationSize = 10000000000;
    
            // user queries
            exports.check_username = prepareSQL('select * from users where username = ?;');
    
            exports.check_nickname = prepareSQL('select * from users where nickname = ?;');
    
            exports.check_email = prepareSQL('select * from users where email = ?;');
    
            exports.insert_user = prepareSQL('insert into users(username, nickname, password_hash, empire_id, guid, email) values(?, ?, ?, null, ?, ?);');
    
            exports.get_user_by_username = prepareSQL('select * from users where username = ?;');
    
            exports.get_user_by_id = prepareSQL('select * from users where id = ?;');
            
            exports.verify_user = prepareSQL('update users set verified = 1 where guid = ? and id = ?;');

                
            // race and empire setup queries
            exports.check_race_name = prepareSQL('select * from races where name = ?;');
    
            exports.check_empire_id = prepareSQL('select empire_id from users where id = ?;');
    
            exports.insert_race = prepareSQL('insert into races(name, lore, survivability, breed_rate, intellegence, combat_instincts) values(?, ?, ?, ?, ?, ?);');
    
            exports.insert_empire = prepareSQL('insert into empires(name, race_id, gc, iron, titanium, uranium, hydrogen, orium, gc_per_hour, planets, military_power, honor)\
                select ?, id, ?, ?, ?, ?, ?, ?, 0, 1, 0, 50 from races where name = ?;');
                
            exports.link_user_to_empire = prepareSQL('update users set empire_id = (select empires.id from empires where empires.name = ?) where id = ?;');

            exports.get_free_planet = prepareSQL('select * from planets where empire_id = 1 order by id asc limit 1;');

            exports.set_home_planet = prepareSQL('update empires set home_planet_id = ? where name = ?;');

            exports.claim_planet_by_empire_name = prepareSQL('update planets set empire_id = (select empires.id from empires where empires.name = ?) where planets.id = ?;');
            
            exports.link_population_to_race_by_race_name = prepareSQL(`update populations set size = ${populationSize}, ready_for_combat = ${populationSize / 1000}, ready_for_combat_support = ${populationSize / 100},\
                mine_1_workers = 0, mine_2_workers = 0, gc_payers = ${populationSize}, state = 85,\
                race_id = (select races.id from races where races.name = ?)\
                where populations.id in (select planets.population_id from planets where planets.id = ?);`);

            exports.check_empire_name = prepareSQL('select * from empires where name = ?;');

            exports.get_login_data_with_empire = prepareSQL('select users.username, users.nickname, empires.name as empire_name, races.name as race_name from users\
                inner join empires on users.empire_id = empires.id\
                inner join races on empires.race_id = races.id\
                where users.id = ?;');

            exports.get_login_data_without_empire = prepareSQL('select username, nickname from users where id = ?;');
    
    
            // planet
            exports.get_planet_by_id = prepareSQL('select planets.*, users.id as owner_id, users.nickname as owner_nickname, empires.name as empire_name,\
                races.name as race_name, r1.pic_path as resource_1_pic_path, r2.pic_path as resource_2_pic_path, planets.development_level, planets.military_power, planets.conquest_time,\
                populations.ready_for_combat, populations.ready_for_combat_support, populations.size, populations.conquered_status, populations.extermination_time,\
                planets.conquest_time > datetime("now", "localtime") as being_conquered,\
                populations.extermination_time > datetime("now", "localtime") as being_exterminated,\
                empires.home_planet_id == planets.id as is_home_planet,\
                populations.size == 0 and empires.id == 1 and races.id == 1 as empty_unoccupied,\
                populations.size == 0 and empires.id != 1 and empires.id != 2 as empty_occupied,\
                populations.size != 0 and empires.id == 2 and races.id == 2 as populated_unoccupied,\
                populations.size != 0 and empires.id != 1 and empires.id != 2 and races.id == empires.race_id as populated_occupied,\
                populations.size != 0 and races.id != empires.race_id and races.id != 2 as conquered_population,\
                populations.size != 0 and races.id != empires.race_id and races.id == 2 as conquered_local_population,\
                empires.id != 1 and empires.id != 2 as has_empire,\
                planets.military_power > 0 as has_army\
                from planets\
                inner join mines on planets.mines_id = mines.id\
                inner join resources r1 on mines.resource_1 = r1.name\
                inner join resources r2 on mines.resource_2 = r2.name\
                inner join empires on planets.empire_id = empires.id\
                inner join populations on planets.population_id = populations.id\
                inner join races on populations.race_id = races.id\
                inner join users on users.empire_id = empires.id\
                where planets.id = ?');

            exports.get_planet_by_id_simple = prepareSQL('select * from planets where id = ?;');

            exports.check_planet_being_conquered = prepareSQL('select conquest_time > datetime("now", "localtime") as being_conquered from planets where id = ?;');

            exports.get_planet_army_military_power = prepareSQL('select ifnull(sum(amount * unit_types.attack), 0) + ifnull(sum(amount * unit_types.defense), 0) as army_military_power\
                from units_in_armies\
                inner join planets on units_in_armies.army_id = planets.army_id\
                inner join unit_types on units_in_armies.unit_type_id = unit_types.id\
                where planets.id = ?;');

            exports.update_planet_military_power = prepareSQL('update planets set military_power = ? where id = ?;');

            exports.get_planet_owner = prepareSQL('select users.id as user_id from planets\
                inner join users on planets.empire_id = users.empire_id\
                where planets.id = ?;');

            exports.get_user_resources = prepareSQL('select gc, iron, hydrogen, titanium, uranium, orium from empires\
                inner join users on empires.id = users.empire_id where users.id = ?;');

            exports.get_user_race = prepareSQL('select races.* from races\
                inner join empires on races.id = empires.race_id\
                inner join users on empires.id = users.empire_id\
                where users.id = ?;');


            // solar system
            exports.get_solar_system_by_id = prepareSQL('select planets.id as planet_id, planets.name as planet_name, planets.texture, empires.name as empire_name,\
                planets.conquest_time > datetime("now", "localtime") as being_conquered from planets\
                inner join empires on planets.empire_id = empires.id\
                where solar_system_id = ?');

            exports.get_planet_solar_system = prepareSQL('select solar_system_id from planets where id = ?;');

            
            // development
            exports.get_planet_development = prepareSQL('select planets.development_level, planets.capacity, mines.max_level,\
                mines.resource_1, mines.resource_2, planets.development_wait_till from planets\
                inner join mines on planets.mines_id = mines.id\
                where planets.id = ?;');

            exports.upgrade_development = prepareSQL('update planets set development_level = (development_level + 1), development_wait_till = ?, capacity = ? where id = ?;');
    
            exports.update_mines_max_level = prepareSQL('update mines set max_level = ? where mines.id in (select mines_id from planets where planets.id = ?);');

    
            // military shop
            exports.get_units_from_type = prepareSQL('select unit_types.id, unit_types.name, unit_types.class_name, unit_types.attack, unit_types.defense, unit_types.gc_per_hour,\
                unit_types.resource_1, unit_types.resource_1_amount, unit_types.resource_2, unit_types.resource_2_amount, unit_types.combat, unit_types.pic_path, unit_types.juba,\
                r1.pic_path as resource_1_pic, r2.pic_path as resource_2_pic from unit_types\
                inner join resources r1 on unit_types.resource_1 = r1.name\
                inner join resources r2 on unit_types.resource_2 = r2.name\
                inner join classes on unit_types.class_name = classes.name\
                where classes.type=?;');
    
            exports.get_military_shop = prepareSQL('select unit_types.id, unit_types.name, unit_types.class_name, unit_types.pic_path, unit_types.juba,\
                r1.pic_path as resource_1_pic, r2.pic_path as resource_2_pic from unit_types\
                inner join resources r1 on unit_types.resource_1 = r1.name\
                inner join resources r2 on unit_types.resource_2 = r2.name;');
            
            exports.get_military_shop_item = prepareSQL('select unit_types.id, unit_types.name, unit_types.class_name,\
                unit_types.attack as unit_attack, unit_types.defense as unit_defense,\
                cast(unit_types.attack * unit_types.juba as int) as attack,\
                cast(unit_types.defense * unit_types.juba as int) as defense,\
                cast(unit_types.resource_1_amount * unit_types.juba as int) as resource_1_amount,\
                cast(unit_types.resource_2_amount * unit_types.juba as int) as resource_2_amount,\
                lower(unit_types.resource_1) as resource_1, unit_types.resource_1_amount as unit_resource_1_amount,\
                lower(unit_types.resource_2) as resource_2, unit_types.resource_2_amount as unit_resource_2_amount,\
                cast(unit_types.gc_per_hour * unit_types.juba as int) as gc_per_hour,\
                unit_types.gc_per_hour as unit_gc_per_hour,\
                unit_types.combat as unit_combat,\
                unit_types.combat_support as unit_combat_support,\
                (unit_types.combat * unit_types.juba) as combat,\
                (unit_types.combat_support * unit_types.juba) as combat_support,\
                unit_types.pic_path, unit_types.juba, unit_types.wait_duration, r1.pic_path as resource_1_pic, r2.pic_path as resource_2_pic from unit_types\
                inner join resources r1 on unit_types.resource_1 = r1.name\
                inner join resources r2 on unit_types.resource_2 = r2.name\
                where unit_types.id = ?;');
    
            exports.check_resources = prepareSQL('select gc, iron, hydrogen, titanium, uranium, orium from empires\
                inner join users on empires.id = users.empire_id\
                where users.id=?;');
    
            exports.add_iron = prepareSQL('update empires set iron = (iron + ?) where empires.id in (select empire_id from users where id = ?);');
    
            exports.add_titanium = prepareSQL('update empires set titanium = (titanium + ?) where empires.id in (select empire_id from users where id = ?);');
    
            exports.add_hydrogen = prepareSQL('update empires set hydrogen = (hydrogen + ?) where empires.id in (select empire_id from users where id = ?);');
            
            exports.add_uranium = prepareSQL('update empires set uranium = (uranium + ?) where empires.id in (select empire_id from users where id = ?);');
            
            exports.add_orium = prepareSQL('update empires set orium = (orium + ?) where empires.id in (select empire_id from users where id = ?);');
    
            exports.add_gc_per_hour = prepareSQL('update empires set gc_per_hour = (gc_per_hour + ?) where empires.id in (select users.empire_id from users where users.id = ?);');
    
            exports.check_people = prepareSQL('select populations.ready_for_combat, populations.ready_for_combat_support from populations\
                inner join planets on planets.population_id = populations.id\
                where planets.id = ?;');
    
            exports.get_max_ready_time = prepareSQL('select units_wait.ready_time from units_wait\
                inner join planets on units_wait.army_id = planets.army_id\
                where planets.id=? and units_wait.ready_time > datetime("now", "localtime") order by units_wait.ready_time desc limit 1;');
    
            // reducing population
            exports.reduce_people = prepareSQL('update populations set ready_for_combat = (ready_for_combat - ?), ready_for_combat_support = (ready_for_combat_support - ?)\
                where populations.id in (select population_id from planets where planets.id = ?);');
    
            // adding people to army
            exports.update_army_people = prepareSQL('update armies set combat_size = (combat_size + ?), combat_support_size = (combat_support_size + ?)\
                where armies.id in (select army_id from planets where planets.id = ?);');
    
            exports.buy_units = prepareSQL('insert into units_wait(unit_type_id, army_id, amount, ready_time) select ?, army_id, ?, ? from planets where planets.id = ?;');
    
            exports.get_home_planet = prepareSQL('select empires.home_planet_id as id, planets.name from empires\
                inner join users on empires.id = users.empire_id\
                inner join planets on empires.home_planet_id = planets.id\
                where users.id = ?;');

            exports.get_all_planets = prepareSQL('select * from planets inner join populations on planets.population_id = populations.id;');
    
    
            // population
            exports.get_planet_population = prepareSQL('select populations.size, planets.capacity, populations.ready_for_combat, populations.ready_for_combat_support,\
                populations.mine_1_workers, populations.mine_2_workers, populations.gc_payers, populations.state, upper(mines.resource_1) as resource_1,\
                upper(mines.resource_2) as resource_2, mines.per_hour_1, mines.per_hour_2, r1.pic_path as resource_1_pic, r2.pic_path as resource_2_pic,\
                mines.level_1 == 0 as no_mine_1, mines.level_2 == 0 as no_mine_2 from populations\
                inner join planets on populations.id = planets.population_id\
                inner join resources r1 on mines.resource_1 = r1.name\
                inner join resources r2 on mines.resource_2 = r2.name\
                inner join mines on planets.mines_id = mines.id\
                where planets.id = ?');
    
            exports.set_planet_population = prepareSQL('update populations set gc_payers=?, mine_1_workers=?, mine_2_workers=? where id in (select population_id from planets where id=?)');
    
    
            // mines
            exports.get_planet_mines = prepareSQL('select mines.resource_1, mines.resource_2, mines.level_1, mines.level_2, mines.per_hour_1, mines.per_hour_2,\
                populations.mine_1_workers, populations.mine_2_workers, mines.max_level, r1.pic_path as resource_1_pic, r2.pic_path as resource_2_pic,\
                mines.level_1 == 0 as no_mine_1, mines.level_2 == 0 as no_mine_2,\
                mines.wait_till_1, mines.wait_till_2 from mines\
                inner join planets on mines.id = planets.mines_id\
                inner join populations on planets.population_id = populations.id\
                inner join resources r1 on mines.resource_1 = r1.name\
                inner join resources r2 on mines.resource_2 = r2.name\
                where planets.id = ?');
               
            exports.add_gc = prepareSQL('update empires set gc = (gc + ?) where empires.id in (select empire_id from users where id = ?);');
    
            exports.upgrade_mine_1 = prepareSQL('update mines set level_1 = (level_1 + 1), per_hour_1 = ?, wait_till_1 = ?\
                where id in (select mines_id from planets where id = ?);');
    
            exports.upgrade_mine_2 = prepareSQL('update mines set level_2 = (level_2 + 1), per_hour_2 = ?, wait_till_2 = ?\
                where id in (select mines_id from planets where id = ?);');
    
    
            // army
            exports.get_planet_units = prepareSQL('select units_in_armies.id, units_in_armies.unit_type_id, unit_types.name, unit_types.pic_path,\
                units_in_armies.amount, unit_types.gc_per_hour as unit_gcph, cast(unit_types.attack * units_in_armies.amount as int) as attack,\
                cast(unit_types.defense * units_in_armies.amount as int) as defense from armies\
                inner join planets on planets.army_id = armies.id\
                inner join units_in_armies on units_in_armies.army_id = armies.id\
                inner join unit_types on units_in_armies.unit_type_id = unit_types.id where planets.id = ?;');
            
            exports.get_planet_units_by_type = prepareSQL('select units_in_armies.id, unit_types.name, unit_types.pic_path, units_in_armies.amount,\
                cast(unit_types.attack * units_in_armies.amount as int) as attack, cast(unit_types.defense * units_in_armies.amount as int) as defense,\
                unit_types.combat as unit_combat, unit_types.combat_support as unit_combat_support,\
                unit_types.attack as unit_attack, unit_types.defense as unit_defense from armies\
                inner join planets on planets.army_id=armies.id\
                inner join units_in_armies on units_in_armies.army_id = armies.id\
                inner join unit_types on units_in_armies.unit_type_id = unit_types.id\
                inner join classes on unit_types.class_name = classes.name\
                where planets.id = ? and classes.type = ?;');
                
            exports.get_planet_army = prepareSQL('select armies.combat_size, armies.combat_support_size, planets.military_power from armies\
                inner join planets on armies.id = planets.army_id\
                where planets.id = ?;');

            exports.get_planet_ready_waiting_units = prepareSQL('select units_wait.unit_type_id, units_wait.amount, units_wait.army_id, unit_types.gc_per_hour as unit_gcph from armies\
                inner join planets on planets.army_id=armies.id\
                inner join units_wait on units_wait.army_id=armies.id\
                inner join unit_types on units_wait.unit_type_id=unit_types.id\
                where planets.id = ? and datetime("now", "localtime") > units_wait.ready_time;');
    
            exports.add_ready_units = prepareSQL('update units_in_armies set amount = (amount + ?) where unit_type_id = ? and army_id = ?;');
    
            exports.add_new_ready_units = prepareSQL('insert into units_in_armies(unit_type_id, army_id, amount) values(?, ?, ?);');
    
            exports.clean_ready_units = prepareSQL('delete from units_wait where army_id in\
                (select planets.army_id from planets where planets.id = ?) and datetime("now", "localtime") > ready_time;');
    
            exports.get_planet_waiting_units_by_type = prepareSQL('select units_wait.id, unit_types.name, unit_types.pic_path, units_wait.amount,\
                cast(unit_types.attack * units_wait.amount as int) as attack, cast(unit_types.defense * units_wait.amount as int) as defense,\
                units_wait.ready_time from armies\
                inner join planets on planets.army_id = armies.id\
                inner join units_wait on units_wait.army_id = armies.id\
                inner join unit_types on units_wait.unit_type_id = unit_types.id\
                inner join classes on unit_types.class_name = classes.name\
                where planets.id=? and datetime("now", "localtime") < units_wait.ready_time and classes.type = ?;');

            exports.update_empire_military_power = prepareSQL('update empires set military_power = (military_power + ?)\
                where empires.id in (select users.empire_id from users where users.id = ?);');
    
            // army dismiss
            exports.get_dismiss_data = prepareSQL('select units_in_armies.amount as max_amount, unit_types.juba, unit_types.combat, unit_types.combat_support, unit_types.gc_per_hour,\
                unit_types.name from units_in_armies\
                inner join unit_types on units_in_armies.unit_type_id = unit_types.id\
                where units_in_armies.id = ?;');
            
            exports.get_army_unit_by_id = prepareSQL('select units_in_armies.amount, units_in_armies.army_id, units_in_armies.id as army_unit_id, unit_types.* from units_in_armies\
                inner join unit_types on units_in_armies.unit_type_id=unit_types.id\
                inner join planets on planets.army_id=units_in_armies.army_id\
                where units_in_armies.id=? and planets.id=?;');
    
            exports.dismiss_people = prepareSQL('update armies set combat_size = (combat_size - ?), combat_support_size = (combat_support_size - ?)\
                where armies.id in (select planets.army_id from planets where planets.id = ?);');
    
            exports.reduce_gcph = prepareSQL('update empires set gc_per_hour = (gc_per_hour - ?) where empires.id in (select users.empire_id from users where users.id = ?);');
    
            exports.add_dismissed_people = prepareSQL('update populations set ready_for_combat = (ready_for_combat + ?), ready_for_combat_support = (ready_for_combat_support + ?)\
                where populations.id in (select planets.population_id from planets where planets.id = ?);');
    
            exports.delete_army_unit = prepareSQL('delete from units_in_armies where id = ?;');
    
            exports.reduce_army_units = prepareSQL('update units_in_armies set amount = (amount - ?) where id = ?;');
            

            // shield
            exports.get_planet_shield = prepareSQL('select shields.*, shields.dome_level == 0 as no_dome, shields.turrets_level == 0 as no_turrets,\
                shields.dome_defense + shields.turrets_attack as shield_military_power from shields\
                inner join planets on shields.id = planets.shield_id where planets.id = ?;');

            exports.upgrade_shield_dome = prepareSQL('update shields set dome_level = (dome_level + 1), dome_defense=?, dome_wait_till = ?\
                where shields.id in (select planets.shield_id from planets where planets.id = ?);');

            exports.repair_shield_dome = prepareSQL('update shields set dome_wait_till = ?, dome_ruined = 0\
                where shields.id in (select planets.shield_id from planets where planets.id = ?);');
    
            exports.upgrade_shield_turrets = prepareSQL('update shields set turrets_level = (turrets_level + 1), turrets_attack=?, turrets_wait_till = ?\
                where shields.id in (select planets.shield_id from planets where planets.id=?);');

            exports.repair_shield_turrets = prepareSQL('update shields set turrets_wait_till = ?, turrets_ruined = 0\
                where shields.id in (select planets.shield_id from planets where planets.id = ?);');


            // loops
            exports.get_user_planets = prepareSQL('select planets.* from planets\
                inner join users on planets.empire_id = users.empire_id\
                where users.id = ? and (planets.conquest_time <= datetime("now", "localtime") or planets.conquest_time is null);');

            exports.get_planet_resource_per_hour_1 = prepareSQL('select mines.per_hour_1 * populations.mine_1_workers as per_hour from planets\
                inner join mines on planets.mines_id = mines.id\
                inner join populations on planets.population_id = populations.id\
                where planets.id=? and mines.resource_1 = ?;');

            exports.get_planet_resource_per_hour_2 = prepareSQL('select mines.per_hour_2 * populations.mine_2_workers as per_hour from planets\
                inner join mines on planets.mines_id = mines.id\
                inner join populations on planets.population_id = populations.id\
                where planets.id = ? and mines.resource_2 = ?;');

            exports.get_planet_gc_per_hour_gain = prepareSQL('select populations.gc_payers from populations\
                inner join planets on planets.population_id = populations.id\
                where planets.id = ?;');
    
            exports.get_empire_gc_per_hour_loss = prepareSQL('select empires.gc, empires.gc_per_hour from empires\
                inner join users on users.empire_id = empires.id\
                where users.id = ?;');
            
            exports.get_all_populations_growth_data = prepareSQL('select populations.id, populations.size, populations.ready_for_combat, populations.ready_for_combat_support,\
                populations.state, races.breed_rate, planets.capacity, populations.mine_1_workers, populations.mine_2_workers from populations\
                inner join races on populations.race_id = races.id\
                inner join planets on planets.population_id = populations.id;');

            exports.breed = prepareSQL('update populations set size = ?, ready_for_combat = ?, ready_for_combat_support = ?, state = ?, gc_payers = ? where id= ?;');


            // profile
            exports.get_user_profile = prepareSQL('select users.email, users.nickname as player_name, empires.name as empire_name, empires.planets, empires.honor,\
                empires.military_power, races.name as race_name, races.survivability, races.breed_rate, races.intellegence, races.combat_instincts from users\
                inner join empires on users.empire_id = empires.id\
                inner join races on empires.race_id = races.id\
                where users.id = ?;');

            exports.get_planet_military_power = prepareSQL('select military_power from planets where id = ?;');

            exports.get_planets_scoreboard = prepareSQL('select users.id as user_id, empires.planets as score, empires.name as empire_name, users.nickname from empires\
                inner join users on empires.id = users.empire_id order by empires.planets desc;');

            exports.get_military_power_scoreboard = prepareSQL('select users.id as user_id, empires.military_power as score, empires.name as empire_name, users.nickname from empires\
                inner join users on empires.id = users.empire_id order by empires.military_power desc;');

            exports.update_user_nickname = prepareSQL('update users set nickname = ? where users.id = ?;');

            exports.update_empire_name = prepareSQL('update empires set empires.name = ? where empires.id in (select users.empire_id from users where users.id = ?);');

            exports.update_race_name = prepareSQL('update races set races.name = ? where race.id in\
                (select empires.race_id from empires inner join users on users.empire_id = empires.id where users.id = ?);');

            exports.check_other_nickname = prepareSQL('select users.* from users where nickname = ? and id != ?;');

            exports.check_other_empire_name = prepareSQL('select empires.* from empires\
                inner join users on empires.id = users.empire_id\
                where empires.name = ? and users.id != ?;');

            exports.check_other_race_name = prepareSQL('select races.* from races\
                inner join empires on races.id = empires.race_id\
                inner join users on empires.id = users.empire_id\
                where races.name = ? and users.id != ?;');

            exports.check_other_email = prepareSQL('select users.* from users where email = ? and id != ?;');

            exports.change_nickname = prepareSQL('update users set nickname = ? where id = ?;');

            exports.change_empire_name = prepareSQL('update empires set name = ? where empires.id in\
                (select users.empire_id from users where users.id = ?);');

            exports.change_race_name = prepareSQL('update races set name = ? where races.id in\
                (select empires.race_id from empires inner join users on empires.id = users.empire_id\
                where users.id = ?);');

            exports.change_email = prepareSQL('update users set email = ? where id = ?;');

            exports.check_email_changed = prepareSQL('select email != ? as changed from users where id = ?;');

            exports.get_user_lore = prepareSQL('select races.lore from races\
                inner join empires on races.id = empires.race_id\
                inner join users on empires.id = users.empire_id\
                where users.id = ?;');

            exports.update_user_lore = prepareSQL('update races set lore = ? where races.id in\
                (select empires.race_id from empires inner join users on users.empire_id = empires.id where users.id = ?);');


            // messages
            exports.get_receiver = prepareSQL('select nickname as receiver_name from users where id = ?;');

            exports.send_message = prepareSQL('insert into messages(sender_id, receiver_id, content, topic, time, read)\
                values(?, (select users.id from users where nickname = ?), ?, ?, datetime("now", "localtime"), 0);');

            exports.get_user_outbox = prepareSQL('select messages.id as message_id, messages.topic,\
                messages.content, messages.time, sender.nickname from messages\
                inner join users sender on sender_id = sender.id\
                where sender_id = ?\
                order by messages.time desc;');

            exports.get_user_inbox = prepareSQL('select messages.id as message_id, messages.topic, messages.read == 0 as unread,\
                messages.content, messages.time, sender.nickname from messages\
                inner join users sender on sender_id = sender.id\
                where receiver_id = ?\
                order by messages.time desc;');

            exports.get_message_by_id = prepareSQL('select messages.id as message_id, messages.topic, messages.content,\
                sender.id as sender_id, receiver.id as receiver_id, sender.nickname as sender_name, receiver.nickname as receiver_name from messages\
                inner join users sender on sender_id = sender.id\
                inner join users receiver on receiver_id = receiver.id\
                where messages.id = ?;');

            exports.get_unread_messages = prepareSQL('select * from messages where receiver_id = ? and read == 0;');

            exports.read_message = prepareSQL('update messages set read = 1 where id = ?;');

            
            // index
            exports.get_classes = prepareSQL('select * from classes;');

            exports.get_resources = prepareSQL('select * from resources;');

            
            // friend list
            exports.get_user_outgoing_requests = prepareSQL('select users2.nickname, users2.id from friends\
                inner join users users2 on user2_id = users2.id\
                where user1_id = ? and accepted = 0;');

            exports.get_user_pending_requests = prepareSQL('select users1.nickname, users1.id from friends\
                inner join users users1 on user1_id = users1.id\
                where user2_id = ? and accepted = 0;');

            exports.get_user_friendlist = prepareSQL('select users2.nickname, users2.id from friends\
                inner join users users2 on user2_id = users2.id\
                where user1_id = ? and friends.accepted = 1\
                union all\
                select users1.nickname, users1.id from friends\
                inner join users users1 on user1_id = users1.id\
                where user2_id = ? and friends.accepted = 1;');
            
            exports.add_friend = prepareSQL('insert into friends(user1_id, user2_id, accepted)\
                values(?, (select users.id from users where nickname = ?), 0);');

            exports.get_all_friends = prepareSQL('select * from friends;');

            exports.check_friend_request = prepareSQL('select * from friends\
                where (user1_id = ? and user2_id = ? and accepted = 0) or\
                (user2_id = ? and user1_id = ? and accepted = 0);');

            exports.delete_request = prepareSQL('delete from friends where user1_id = ? and user2_id = ? and accepted = 0;');

            exports.accept_request = prepareSQL('update friends set accepted = 1 where user1_id = ? and user2_id = ?;');

            exports.delete_friend = prepareSQL('delete from friends\
                where (user1_id = ? and user2_id = ? and accepted = 1) or\
                (user2_id = ? and user1_id = ? and accepted = 1);');

            exports.check_friends = prepareSQL('select * from friends\
                where (user1_id = ? and user2_id = ? and accepted = 1) or\
                (user2_id = ? and user1_id = ? and accepted = 1);');

            
            // conquest
            exports.check_home_planet = prepareSQL('select * from empires where home_planet_id = ?;');

            exports.get_planet_required_military_power = prepareSQL('select military_power_requirement from planets where id = ?;');

            exports.can_populate = prepareSQL('select (select races.survivability from races\
                inner join empires on empires.race_id = races.id\
                inner join users on users.empire_id = empires.id\
                where users.id = ?) >=\
                (select planets.survivability_requirement from planets where planets.id = ?)\
                as can_populate;');

            exports.check_planet_army = prepareSQL('select military_power != 0 as has_army from planets where id = ?;');

            exports.get_conquest_unit = prepareSQL('select unit_types.combat as unit_combat, unit_types.combat_support as unit_combat_support, units_in_armies.amount,\
                unit_types.id as unit_type_id, units_in_armies.id as unit_id, planets.id as planet_id, armies.id as army_id,\
                (unit_types.combat * units_in_armies.amount) as combat,\
                (unit_types.combat_support * units_in_armies.amount) as combat_support,\
                (unit_types.attack * units_in_armies.amount) as attack,\
                (unit_types.defense * units_in_armies.amount) as defense,\
                units_in_armies.amount >= ?\
                and users.id = ?\
                and (planets.conquest_time <= datetime("now", "localtime") or planets.conquest_time is null)\
                as valid\
                from units_in_armies\
                inner join unit_types on units_in_armies.unit_type_id = unit_types.id\
                inner join armies on units_in_armies.army_id = armies.id\
                inner join planets on armies.id = planets.army_id\
                inner join users on users.empire_id = planets.empire_id\
                where units_in_armies.id = ?;');

            exports.check_military_power = prepareSQL('select military_power_requirement <= ? as military_power_suffies from planets where id = ?;');

            exports.conquer_planet = prepareSQL('update planets\
                set last_empire_id = empire_id,\
                empire_id = (select users.empire_id from users where users.id = ?),\
                conquest_time = ?\
                where planets.id = ?;');

            exports.change_conquered_population_status = prepareSQL('update populations set conquered_status = ? where id = (select population_id from planets where planets.id = ?);');

            exports.check_planet_population = prepareSQL('select populations.race_id != 1 as populated from populations\
                inner join planets on populations.id = planets.population_id\
                where planets.id = ?');
            
            exports.get_planet_loot_data = prepareSQL('select planets.development_level, planets.name as planet_name, planets.empire_id, users.id as user_id, mines.resource_1, mines.resource_2 from planets\
                inner join mines on planets.mines_id = mines.id\
                inner join users on users.empire_id = planets.empire_id\
                where planets.id = ?;');

            exports.reduce_combat_and_combat_support = prepareSQL('update armies\
                set combat_size = combat_size - ?,\
                combat_support_size = combat_support_size - ?\
                where id = ?;');

            exports.add_combat_and_combat_support = prepareSQL('update armies\
                set combat_size = combat_size + ?,\
                combat_support_size = combat_support_size + ?\
                where id = (select planets.army_id from planets where planets.id = ?);');

            exports.move_all_units_to_planet_army = prepareSQL('update units_in_armies\
                set army_id = (select planets.army_id from planets where planets.id = ?)\
                where id = ?;'); 

            exports.remove_units_from_army = prepareSQL('delete from units_in_armies where id = ?;');

            exports.check_unit_in_army = prepareSQL('select id from units_in_armies where unit_type_id = ? and army_id = (select planets.army_id from planets where planets.id = ?);');

            exports.add_existing_units_to_army = prepareSQL('update units_in_armies set amount = amount + ? where unit_type_id = ? and army_id = (select planets.army_id from planets where planets.id = ?);');

            exports.add_new_units_to_army = prepareSQL('insert into units_in_armies(amount, unit_type_id, army_id) values(?, ?, (select army_id from planets where id = ?));');

            exports.set_planet_conquest_tme = prepareSQL('update planets set conquest_time = ? where id = ?;');

            exports.remove_planet_waiting_units = prepareSQL('delete from units_wait where army_id = (select planets.army_id from planets where planets.id = ?);');


            // population choice
            exports.make_population_peace = prepareSQL('update populations set conquered_status = "peace" where id = (select population_id from planets where planets.id = ?);');
            
            exports.enslave_population = prepareSQL('update populations set conquered_status = "slavery" where id = (select population_id from planets where planets.id = ?);');

            exports.exterminate_population = prepareSQL('update populations set\
                size = 0, ready_for_combat = 0, ready_for_combat_support = 0, mine_1_workers = 0, mine_2_workers = 0, gc_payers = 0, state = 0, extermination_time = ?, conquered_status = null,\
                race_id = (\
                    select empires.race_id from empires\
                    inner join users on users.empire_id = empires.id\
                    where users.id = ?)\
                where id = (select planets.population_id from planets where planets.id = ?);');

            exports.get_population_size = prepareSQL('select (populations.size + populations.ready_for_combat + populations.ready_for_combat_support) as size from populations\
                inner join planets on planets.population_id = populations.id\
                where planets.id = ?;');

            exports.get_conquered_population_status = prepareSQL('select populations.conquered_status from populations\
                inner join planets on populations.id = planets.population_id\
                where planets.id = ?;');

            
            // notifications
            exports.add_notification = prepareSQL('insert into notifications(user_id, reveal_time, notification, viewed) values(?, ?, ?, 0);');

            exports.get_user_notifications = prepareSQL('select * from notifications\
                where user_id = ? and reveal_time < datetime("now", "localtime")\
                order by reveal_time desc;');

            exports.get_user_unviewed_notifications = prepareSQL('select * from notifications\
                where user_id = ? and reveal_time < datetime("now", "localtime") and viewed = 0;');

            exports.view_user_notifications = prepareSQL('update notifications set viewed = 1 where user_id = ? and reveal_time < datetime("now", "localtime");');


            // transfers
            exports.add_transfer = prepareSQL('insert into transfers(resource_name, amount, user_id, time) values(?, ?, ?, ?);');

            exports.get_user_transfers = prepareSQL('select * from transfers where user_id = ?;');

            exports.delete_old_user_transfers = prepareSQL('delete from transfers where user_id = ? and time < datetime("now", "localtime")');


            // battles
            
                
            // tests
            exports.get_user_orium = prepareSQL('select empires.orium from empires inner join users on empires.id = users.empire_id where users.username = ?;'); 
    
            exports.get_user_intellegence = prepareSQL('select races.intellegence from races\
                inner join empires on empires.race_id = races.id\
                inner join users on users.empire_id = empires.id\
                where users.username = ?');
    
            exports.get_all_empires = prepareSQL('select * from empires;');
    
            exports.get_all_users = prepareSQL('select * from users;');

            exports.get_all_races = prepareSQL('select * from races;');

            exports.get_all_populations = prepareSQL('select * from populations;');

            exports.get_all_messages = prepareSQL('select * from messages;');

            exports.get_all_notifications = prepareSQL('select * from notifications');


            exports.db = db;
    
            // delete all tables      
            /*db.each("select name from sqlite_master where type='table'", function (err, table) {
                //throw "you sure?";
                if (table.name != 'sqlite_sequence')// && table.name != 'users')      
                    db.run('drop table ' + table.name + ';');
            });*/
        });
    } catch (e) {
        console.log(e);
    }
}
 