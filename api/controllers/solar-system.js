const fs = require('fs');
const db_api = require('../database/db-api');
const errors = require('../helpers/errors');
const codes = require('../helpers/status-codes');
const Error = errors.Error;


exports.get_solar_system = async function(req, res, next) {  
    try {
        const solarSystem = await db_api.get_solar_system_by_id.all(req.params.id);
        const resources = await db_api.get_user_resources.get(req.session.user_id);
        const planetsList = (await db_api.get_user_planets.all(req.session.user_id)).filter(function(item) {
            return item.id != req.session.planet_id;
        });        

        res.render('solar-system', {
            planetsList: planetsList,
            planetName: req.session.planet_name,
            planetID: req.session.planet_id,
            planets: solarSystem,
            userID: req.session.user_id,
            resources: resources
        });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_planet_solar_system = async function(req, res, next) {
    try {
        const solarSystem = await db_api.get_planet_solar_system.get(req.session.planet_id);

        res.redirect('/solar-system/' + solarSystem.solar_system_id);

    } catch (e) {
        return errors.resError(res, e);
    }
}