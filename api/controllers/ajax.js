const fs = require('fs');
const request = require('request');
const guid = require('guid');
const mailer = require('../helpers/mailer')
const db_api = require('../database/db-api');
const helper = require('../helpers/helper');
const errors = require('../helpers/errors');
const formulas = require('../helpers/formulas');
const notifications = require('../helpers/notifications');
const codes = require('../helpers/status-codes');
const Error = errors.Error;  // instead of using errors.Error every time


exports.get_race_stats = function(req, res, next) {    
    fs.readFile('views/ajax/race-stats.html', function(err, data) {
        if (err) return errors.resError(res, new Error(codes.serverError, errors.viewError));

        res.send(data.toString());
    });
} 

exports.post_race = async function(req, res, next) {
    try {
        helper.outOfLimits(req.body, 'raceName', 3, 16, new Error(codes.badRequest, errors.invalidRaceNameLengthError));
        helper.outOfLimits(req.body, 'empireName', 3, 16, new Error(codes.badRequest, errors.invalidEmpireNameLengthError));
        helper.nonIntFields(req.body, 'survivability', 'breedRate', 'intellegence', 'combatInstincts', new Error(codes.badRequest, errors.invalidValueError));

        // if user tries to get extra points.
        if (req.body.survivability + req.body.breedRate + req.body.intellegence + req.body.combatInstincts > 25) throw new Error(codes.badRequest, errors.InvalidActionError);

        // make request to verify captcha
        const verifyURL = 'https://www.google.com/recaptcha/api/siteverify?secret=' +
            process.env.RECAPTCHA_SECRET + '&response=' + req.body.captcha + '&remoteip=' + req.connection.remoteAddress;
        
        request(verifyURL, async function(err, response, body) {
            try {
                console.log(err);
                // checking if request worked
                if (err) throw new Error(codes.serverError, errors.internalCaptchaError);
                if (!JSON.parse(body).success) throw new Error(codes.badRequest, errors.userCaptchaError);                

                const raceName = req.body.raceName.trim();
                const empireName = req.body.empireName.trim();
                const lore = req.body.lore.trim();

                // checking if name exists
                const raceNameCheck = await db_api.check_race_name.get(raceName);
                if (raceNameCheck) throw new Error(codes.badRequest, errors.raceNameTakenError);

                const empireNameCheck = await db_api.check_empire_name.get(empireName);
                if (empireNameCheck) throw new Error(codes.badRequest, errors.empireNameTakenError);

                // inserting race
                await db_api.insert_race.run(raceName, lore, req.body.survivability, req.body.breedRate, req.body.intellegence, req.body.combatInstincts);

                // inserting empire
                await db_api.insert_empire.run(empireName, 99999999, 9999999999, 999999999, 999999999, 999999999, 999999999, raceName);

                // linking race and empire
                await db_api.link_user_to_empire.run(empireName, req.session.user_id);

                // setting home planet
                const planet = await db_api.get_free_planet.get();

                await db_api.claim_planet_by_empire_name.run(empireName, planet.id);

                await db_api.set_home_planet.run(planet.id, empireName);

                await db_api.link_population_to_race_by_race_name.run(raceName, planet.id);

                await helper.login(req, req.session.user_id);

                await notifications.welcome(req.session.user_id, -1);

                res.status(codes.ok).json({
                    msg: 'OK'
                }); 

            } catch (e) {
                return errors.resError(res, e);
            }

        });

    } catch (e) {
        return errors.resError(res, e);
    }
}

// we split races to name/lore and stats. first the user chooses a name and it has to be checked, otherwise he will have to refill the whole form.
// so we made this route to check the name.the name will be checked again later on after submitting the race stats+name.
exports.post_race_name_check = async function(req, res, next) {
    try {
        helper.outOfLimits(req.body, 'raceName', 3, 16, new Error(codes.badRequest, errors.invalidRaceNameLengthError));
        helper.outOfLimits(req.body, 'empireName', 3, 16, new Error(codes.badRequest, errors.invalidEmpireNameLengthError));

        const raceName = req.body.raceName.trim();
        const empireName = req.body.empireName.trim();

        // checking if race and empire name are taken
        const raceNameCheck = await db_api.check_race_name.get(raceName);
        if (raceNameCheck) throw new Error(codes.badRequest, errors.raceNameTakenError);

        const empireNameCheck = await db_api.check_empire_name.get(empireName);
        if (empireNameCheck) throw new Error(codes.badRequest, errors.empireNameTakenError);

        res.status(codes.ok).json({
            msg: 'OK'
        });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_development = async function(req, res, next) {
    try {
        const development = await db_api.get_planet_development.get(req.session.planet_id);
        
        development.upgrade_gc_cost = formulas.developmentUpgradeGcCost(development.development_level);
        development.upgrade_iron_cost = formulas.developmentUpgradeIronCost(development.development_level);
        development.mines_next_max_level = formulas.minesNextMaxLevel(development.development_level);
        development.next_capacity = formulas.planetNextCapacity(development.development_level);

        const wait = development.development_wait_till && new Date(development.development_wait_till) > new Date();
        
        res.render('ajax/development', { item: development, wait: wait });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_development_upgrade = async function(req, res, next) {
    try {
        const development = await db_api.get_planet_development.get(req.session.planet_id);

        if (development.development_wait_till && new Date(development.development_wait_till) > new Date()) throw new Error(codes.badRequest, errors.invalidActionError);

        // resource check
        const resources = await db_api.check_resources.get(req.session.user_id);   

        const upgradeGcCost = formulas.developmentUpgradeGcCost(development.development_level);
        const upgradeIronCost = formulas.developmentUpgradeIronCost(development.development_level);        

        if (upgradeGcCost > resources.gc) throw new Error(codes.badRequest, errors.insufficientGcError);        
        if (upgradeIronCost > resources.iron) throw new Error(codes.badRequest, errors.insufficientIronError); 

        // reducing gc and iron
        await helper.addResource('gc', -1 * upgradeGcCost, req.session.user_id);
        await helper.addResource('iron', -1 * upgradeIronCost, req.session.user_id);

        // buying upgrade
        let newWaitTill = new Date();
        const upgradeTime = formulas.developmentUpgradeTime(development.development_level);

        newWaitTill.setSeconds(newWaitTill.getSeconds() + upgradeTime);
        
        const capacity = formulas.planetCapacity(development.development_level + 1);       
        await db_api.upgrade_development.run(helper.formatDate(newWaitTill), capacity, req.session.planet_id);

        const minesMaxLevel = formulas.minesMaxLevel(development.development_level + 1);
        await db_api.update_mines_max_level.run(minesMaxLevel, req.session.planet_id);

        await otifications.developmentUpgrade(req.session.user_id, upgradeTime, req.session.planet_name);

        res.status(codes.ok).json({
            msg: 'OK',
            costData: [
                { name: 'gc', amount: upgradeGcCost },
                { name: 'iron', amount: upgradeIronCost }
            ]
        });
        
    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_anti_shield = async function(req, res, next) {
    try {
        const units = await db_api.get_units_from_type.all('Anti Shield');

        res.render('ajax/military-shop', { items: units }); 

    } catch (e) {
        return errors.resError(res, e);
    } 
}

exports.get_space_fleet = async function(req, res, next) {
    try {
        const units = await db_api.get_units_from_type.all('Space Fleet');

        res.render('ajax/military-shop', { items: units }); 

    } catch (e) {
        return errors.resError(res, e);      
    }  
}

exports.get_ground_forces = async function(req, res, next) {
    try {
        const units = await db_api.get_units_from_type.all('Ground Forces');

        res.render('ajax/military-shop', { items: units });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_military_shop = async function(req, res, next) {
    try {
        const units = await db_api.get_military_shop.all();
        
        res.render('ajax/military-shop', { items: units });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_military_shop_item = async function(req, res, next) {
    try {
        const unit = await db_api.get_military_shop_item.get(req.params.id);

        res.render('ajax/military-shop-item', { item: unit });

    } catch (e) {
        return errors.resError(res, e);
    }        
}

exports.post_military_shop_buy = async function(req, res, next) {  
    try {
        helper.nonIntFields(req.body, 'id', 'amount', new Error(codes.badRequest, errors.invalidValueError));
        helper.nonPositiveFields(req.body, 'amount', new Error(codes.badRequest, errors.invalidValueError));

        const unit = await db_api.get_military_shop_item.get(req.body.id);

        const resource_1_amount = req.body.amount * unit.unit_resource_1_amount;
        const resource_2_amount = req.body.amount * unit.unit_resource_2_amount;
        const combat = req.body.amount * unit.unit_combat;
        const combatSupport = req.body.amount * unit.unit_combat_support;
        const gcph = req.body.amount * unit.unit_gc_per_hour;

        // resource check
        let resourceCheck = await db_api.check_resources.get(req.session.user_id);
        
        if (resourceCheck[unit.resource_1.toLowerCase()] < resource_1_amount ||
            resourceCheck[unit.resource_2.toLowerCase()] < resource_2_amount)
            throw new Error(codes.badRequest, errors.insufficientResourceError);

        // population check
        let peopleCheck = await db_api.check_people.get(req.session.planet_id);    

        if (peopleCheck.ready_for_combat < combat &&
            peopleCheck.ready_for_combat < combatSupport)            
            throw new Error(codes.badRequest, errors.insufficientCombatAndSupportReadyError);
    
        if (peopleCheck.ready_for_combat < combat) throw new Error(codes.badRequest, errors.insufficientCombatReadyError);

        if (peopleCheck.ready_for_combat_support < combatSupport) throw new Error(codes.badRequest, errors.insufficientSupportReadyError);        

        // resource reduction
        await helper.addResource(unit.resource_1, -1 * resource_1_amount, req.session.user_id);
        await helper.addResource(unit.resource_2, -1 * resource_2_amount, req.session.user_id);
        
        // reducing people
        await db_api.reduce_people.run(combat, combatSupport, req.session.planet_id);

        // adding people to army
        await db_api.update_army_people.run(combat, combatSupport, req.session.planet_id);
        
        // getting unit ready time
        const maxReadyTime = await db_api.get_max_ready_time.get(req.session.planet_id);
        let readyTime;        
        
        if (maxReadyTime) {
            readyTime = new Date(maxReadyTime.ready_time);  // if there is a unit in the queue   
        } else {  
            readyTime = new Date(); // if there is no unit in the queue  
        } 

        // buying the units
        readyTime.setSeconds(readyTime.getSeconds() + unit.wait_duration * req.body.amount);
        
        await db_api.buy_units.run(req.body.id, req.body.amount, helper.formatDate(readyTime), req.session.planet_id); 
        
        res.status(codes.ok).json({
            msg: 'OK',
            costData: [ 
                { name: unit.resource_1.toLowerCase(), amount: resource_1_amount },
                { name: unit.resource_2.toLowerCase(), amount: resource_2_amount }
            ]
        });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_population = async function(req, res, next) {
    try {
        const population = await db_api.get_planet_population.get(req.session.planet_id);

        res.render('ajax/population', { item: population }); 

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.post_population = async function(req, res, next) {
    try {
        helper.nonIntFields(req.body, 'resource1', 'resource2', new Error(codes.badRequest, errors.invalidValueError));
        helper.negativeFields(req.body, 'resource1', 'resource2', new Error(codes.badRequest, errors.invalidValueError));

        const population = await db_api.get_planet_population.get(req.session.planet_id);
        
        // checking if the player is a hackser
        if (req.body.resource1 + req.body.resource2 > population.size) throw new Error(codes.badRequest, errors.invalidActionError);
        
        await db_api.set_planet_population.run(
            population.size - req.body.resource1 - req.body.resource2,
            req.body.resource1, req.body.resource2,
            req.session.planet_id
        );

        res.status(codes.ok).json({
            msg: 'OK'
        });

    } catch (e) {
        return errors.resError(res, e);
    }   
}

exports.get_mines = async function(req, res, next) {
    try {
        let mines = await db_api.get_planet_mines.get(req.session.planet_id);

        const wait1 = mines.wait_till_1 !== null && new Date(mines.wait_till_1) > new Date();
        const wait2 = mines.wait_till_2 !== null && new Date(mines.wait_till_2) > new Date();

        mines.upgrade_cost_1 = formulas.mineUpgradeCost(mines.level_1);
        mines.upgrade_cost_2 = formulas.mineUpgradeCost(mines.level_2);

        res.render('ajax/mines', { item: mines, wait1: wait1, wait2: wait2 });  

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_mines_upgrade = async function(req, res, next) {
    try {
        if (req.params.id != 1 && req.params.id != 2) throw new Error(codes.badRequest, errors.invalidActionError);

        const mines = await db_api.get_planet_mines.get(req.session.planet_id);

        const mine = {
            resource: mines['resource_' + req.params.id],
            level: mines['level_' + req.params.id],
            wait_till: mines['wait_till_' + req.params.id], 
            max_level: mines['max_level']
        }
    
        // if the mine is not ready
        if (mine.wait_till && new Date(mine.wait_till) > new Date()) throw new Error(codes.badRequest, errors.invalidActionError);

        // checking if the mine has already reached max level.
        if (mine.max_level <= mine.level) throw new Error(codes.badRequest, errors.mineMaxLevelError);
        
        // checking gc
        const resourcesCheck = await db_api.check_resources.get(req.session.user_id);

        const upgradeCost = formulas.mineUpgradeCost(mine.level);

        if (resourcesCheck.gc < upgradeCost) throw new Error(codes.badRequest, errors.insufficientGcError);
        
        // reducing gc        
        await helper.addResource('gc', -1 * upgradeCost, req.session.user_id);

        // upgrading mine (1 or 2)
        const upgradeTime = formulas.mineUpgradeTime(mine.level);       
        let newWaitTill = new Date();

        newWaitTill.setSeconds(newWaitTill.getSeconds() + upgradeTime);

        const nextPerHour = formulas.minePerHour(mine.level + 1);
        
        if (req.params.id == 1) {
            await db_api.upgrade_mine_1.run(nextPerHour, helper.formatDate(newWaitTill), req.session.planet_id);
        } else {
            await db_api.upgrade_mine_2.run(nextPerHour, helper.formatDate(newWaitTill), req.session.planet_id);           
        }

        await notifications.mineUpgrade(req.session.user_id, upgradeTime, req.session.planet_name, mine.resource);

        res.status(codes.ok).json({
            msg: 'OK',
            costData: [
                { name: 'gc', amount: upgradeCost }
            ]
        }); 

    } catch (e) {
        return errors.resError(res, e);
    }    
}

exports.get_army = async function(req, res, next) {
    try {
        // moves ready units from waiting table to ready table
        await helper.updatePlanetArmy(req.session.planet_id);     
        
        const armyData = await db_api.get_planet_army.get(req.session.planet_id);  // this returns combat, combat_support, military_power
        
        // ready army categories
        const antiShieldRows = await db_api.get_planet_units_by_type.all(req.session.planet_id, 'Anti Shield');
    
        const spaceRows = await db_api.get_planet_units_by_type.all(req.session.planet_id, 'Space Fleet');

        const groundRows = await db_api.get_planet_units_by_type.all(req.session.planet_id, 'Ground Forces');

        // waiting army categories
        const antiShieldWaitRows = await db_api.get_planet_waiting_units_by_type.all(req.session.planet_id, 'Anti Shield');              

        const spaceWaitRows = await db_api.get_planet_waiting_units_by_type.all(req.session.planet_id, 'Space Fleet');

        const groundWaitRows = await db_api.get_planet_waiting_units_by_type.all(req.session.planet_id, 'Ground Forces');

        res.render('ajax/army', { 
            army: armyData,
            antiShield: antiShieldRows, 
            space: spaceRows, 
            ground: groundRows,
            antiShieldWait: antiShieldWaitRows,
            spaceWait: spaceWaitRows,
            groundWait: groundWaitRows 
        });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_army_dismiss = async function(req, res, next) {
    try {
        const dismiss = await db_api.get_dismiss_data.get(req.params.id);

        res.render('ajax/army-dismiss', { item: dismiss });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_army_dismiss_run = async function(req, res, next) {
    try {
        helper.nonIntFields(req.params, 'amount', errors.invalidValueError);
        helper.nonPositiveFields(req.params, 'amount', errors.invalidValueError);

        await helper.dismissUnit(req.params.id, req.session.planet_id, req.params.amount, req.session.user_id);

        await helper.updatePlanetArmy(req.session.planet_id);

        res.status(codes.ok).json({
            msg: 'OK'
        });

    } catch (e) {        
        return errors.resError(res, e);
    }
}

exports.get_shield = async function(req, res, next) {
    try {
        const shield = await db_api.get_planet_shield.get(req.session.planet_id);

        shield.dome_upgrade_cost = formulas.domeUpgradeCost(shield.dome_level);
        shield.turrets_upgrade_cost = formulas.turretsUpgradeCost(shield.turrets_level);
        shield.dome_repair_cost = formulas.domeRepairCost(shield.dome_level);
        shield.turrets_repair_cost = formulas.turretsRepairCost(shield.turrets_level);

        const domeWait = shield.dome_wait_till !== null && new Date(shield.dome_wait_till) > new Date();
        const turretsWait = shield.turrets_wait_till !== null && new Date(shield.turrets_wait_till) > new Date();

        res.render('ajax/shield', { item: shield, domeWait: domeWait, turretsWait: turretsWait });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.post_dome_action = async function(req, res, next) {
    try {           
        const action = req.body.action;

        if (action != 'upgrade' && action != 'repair') throw new Error(codes.badRequest, errors.invalidActionError); 
         
        const shield = await db_api.get_planet_shield.get(req.session.planet_id);
        
        // checks
        if (action == 'upgrade' && shield.dome_ruined ||
            action == 'repair' && !shield.dome_ruined ||
            (shield.dome_wait_till && new Date(shield.dome_wait_till) > new Date()))
            throw new Error(codes.badRequest, errors.invalidActionError);  

        // resource check
        const resourceCheck = await db_api.check_resources.get(req.session.user_id);
        let cost;

        if (action == 'upgrade') {
            cost = formulas.domeUpgradeCost(shield.dome_level);   
        } else {
            cost = formulas.domeRepairCost(shield.dome_level);   
        }        
        
        if (resourceCheck.orium < cost) throw new Error(codes.badRequest, errors.insufficientOriumError); 
            
        // buying process
        await helper.addResource('orium', -1 * cost, req.session.user_id);

        let newWaitTill = new Date();
        
        if (action == 'upgrade') {
            let time = formulas.domeUpgradeTime(shield.dome_level);  
            newWaitTill.setSeconds(newWaitTill.getSeconds() + time); 
            
            const nextDefense = formulas.domeDefense(shield.dome_level + 1);
            
            await db_api.upgrade_shield_dome.run(nextDefense, helper.formatDate(newWaitTill), req.session.planet_id);  
            await notifications.domeUpgrade(req.session.user_id, time, req.session.planet_name);        
        } else {
            let time = formulas.domeRepairTime(shield.dome_level);  
            newWaitTill.setSeconds(newWaitTill.getSeconds() + tme); 

            await db_api.repair_shield_dome.run(helper.formatDate(newWaitTill), req.session.planet_id);
            await notifications.domeRepair(req.session.user_id, time, req.session.planet_name);  
        } 

        await helper.updatePlanetArmy(req.session.planet_id);   
        
        res.status(codes.ok).json({
            msg: 'OK',
            costData: [ { name: 'orium', amount: cost } ]
        });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.post_turrets_action = async function(req, res, next) {
    try {   
        const action = req.body.action;
    
        if (action != 'upgrade' && action != 'repair') throw new Error(codes.badRequest, errors.invalidActionError); 
         
        const shield = await db_api.get_planet_shield.get(req.session.planet_id);
        
        // checks
        if (action == 'upgrade' && shield.turrets_ruined ||
            action == 'repair' && !shield.turrets_ruined ||
            (shield.turrets_wait_till && new Date(shield.turrets_wait_till) > new Date())) 
            throw new Error(codes.badRequest, errors.invalidActionError); 

        // resource check
        const resourceCheck = await db_api.check_resources.get(req.session.user_id);
        let cost;

        if (action == 'upgrade') {
            cost = formulas.turretsUpgradeCost(shield.turrets_level);   
        } else {
            cost = formulas.turretsRepairCost(shield.turrets_level);    
        }        
        
        if (resourceCheck.uranium < cost) throw new Error(codes.badRequest, errors.insufficientUraniumError); 
            
        // buying process
        await helper.addResource('uranium', -1 * cost, req.session.user_id);

        let newWaitTill = new Date();
        
        if (action == 'upgrade') {
            let time = formulas.turretsUpgradeTime(shield.turrets_level);  
            newWaitTill.setSeconds(newWaitTill.getSeconds() + time); 

            const nextAttack = formulas.turretsAttack(shield.turrets_level + 1);
            
            await db_api.upgrade_shield_turrets.run(nextAttack, helper.formatDate(newWaitTill), req.session.planet_id);
            await notifications.turretsUpgrade(req.session.user_id, time, req.session.planet_name);             
        } else {
            let time = formulas.turretsRepairTime(shield.turrets_level); 
            newWaitTill.setSeconds(newWaitTill.getSeconds() + time);             

            await db_api.repair_shield_turrets.run(helper.formatDate(newWaitTill), req.session.planet_id);
            await notifications.turretsRepair(req.session.user_id, time, req.session.planet_name);         
        }   

        await helper.updatePlanetArmy(req.session.planet_id);  
        
        res.status(codes.ok).json({
            msg: 'OK',
            costData: [ { name: 'uranium', amount: cost } ]
        });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_self_profile = async function(req, res, next) {
    try {
        // get ranks
        const planetsRank = await helper.getPlanetsRank(req.session.user_id);
        const militaryRank = await helper.getMilitaryRank(req.session.user_id);

        const profile = await db_api.get_user_profile.get(req.session.user_id);

        res.render('ajax/self-profile', { item: profile, planets_rank: planetsRank, military_rank: militaryRank });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.post_self_profile = async function(req, res, next) {
    try {        
        helper.outOfLimits(req.body, 'raceName', 3, 16, new Error(codes.badRequest, errors.invalidRaceNameLengthError));
        helper.outOfLimits(req.body, 'empireName', 3, 16, new Error(codes.badRequest, errors.invalidEmpireNameLengthError));
        helper.outOfLimits(req.body, 'nickname', 3, 16, new Error(codes.badRequest, errors.invalidNicknameLengthError));

        const nickname = req.body.nickname.trim();
        const raceName = req.body.raceName.trim();
        const empireName = req.body.empireName.trim();
        const email = req.body.email.trim();
        const userGuid = guid.create();

        // checking changes
        const nicknameCheck = await db_api.check_other_nickname.get(nickname, req.session.user_id);
        if (nicknameCheck) throw new Error(codes.badRequest, errors.nicknameTakenError);

        const raceNameCheck = await db_api.check_other_race_name.get(raceName, req.session.user_id);
        if (raceNameCheck) throw new Error(codes.badRequest, errors.raceNameTakenError);

        const empireNameCheck = await db_api.check_other_empire_name.get(empireName, req.session.user_id);
        if (empireNameCheck) throw new Error(codes.badRequest, errors.empireNameTakenError);

        const emailNameCheck = await db_api.check_other_email.get(email, req.session.user_id);
        if (emailNameCheck) throw new Error(codes.badRequest, errors.emailTakenError);

        await db_api.change_nickname.run(nickname, req.session.user_id);
        await db_api.change_empire_name.run(empireName, req.session.user_id);
        await db_api.change_race_name.run(raceName, req.session.user_id);

        const emailChanged = await db_api.check_email_changed.get(email, req.session.user_id);

       await helper.login(req, req.session.user_id);

        res.status(codes.ok).json({
            msg: 'OK'
        });

        if (emailChanged.changed) {
            await db_api.change_email.run(email, req.session.user_id);
            await mailer.send(email, 'Email confirmation', 'Press this link to confirm your email:\nhttp://localhost:9292/user/email-confirm/' + userGuid.value);
        }

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_profile = async function(req, res, next) {
    try {
        // get ranks
        const planetsRank = await helper.getPlanetsRank(req.params.id);
        const militaryRank = await helper.getMilitaryRank(req.params.id);

        const profile = await db_api.get_user_profile.get(req.params.id);

        res.render('ajax/profile', { item: profile, planets_rank: planetsRank, military_rank: militaryRank });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_self_lore = async function(req, res, next) {
    try {
        const lore = await db_api.get_user_lore.get(req.session.user_id);

        res.render('ajax/self-lore', { item: lore });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.post_self_lore = async function(req, res, next) {
    try {   
        await db_api.update_user_lore.run(req.body.lore, req.session.user_id);  // if lore is undefined an error will be thrown.

        res.status(codes.ok).json({
            msg: 'OK'
        });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_lore = async function(req, res, next) {
    try {
        const lore = await db_api.get_user_lore.get(req.params.id);

        res.render('ajax/lore', { item: lore });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_make_message_with_recipient = async function(req, res, next) {
    try {
        const user = await db_api.get_receiver.get(req.params.id);

        res.render('ajax/make-message', { item: user });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_make_message = async function(req, res, next) {
    try {
        res.render('ajax/make-message');

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.post_make_message = async function(req, res, next) {
    try {
        helper.outOfLimits(req.body, 'recipient', 3, 16, errors.recipientEmptyError);
        helper.outOfLimits(req.body, 'topic', 1, 64, errors.invalidTopicLengthError);
        helper.outOfLimits(req.body, 'message', 1, 1024, errors.messageEmptyError);

        const recipient = req.body.recipient.trim();
        const topic = req.body.topic.trim();
        const message = req.body.message;

        // checking if recipient exists
        const nicknameCheck = await db_api.check_nickname.get(recipient);
        if (!nicknameCheck) throw new Error(codes.badRequest, errors.userNotFoundError);

        // checking if user mails himself.
        if (nicknameCheck.id == req.session.user_id) throw new Error(codes.badRequest, errors.butWhyError);

        await db_api.send_message.run(req.session.user_id, recipient, message, topic);

        res.status(codes.ok).json({
            msg: 'OK'
        });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_inbox = async function(req, res, next) {
    try {
        const inbox = await db_api.get_user_inbox.all(req.session.user_id);

        res.render('ajax/inbox', { messages: inbox });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_outbox = async function(req, res, next) {
    try {
        const outbox = await db_api.get_user_outbox.all(req.session.user_id);
        
        res.render('ajax/outbox', { messages: outbox });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_inbox_message = async function(req, res, next) {
    try {
        const message = await db_api.get_message_by_id.get(req.params.id);
        
        if (message.receiver_id != req.session.user_id) throw new Error(codes.badRequest, errors.messageAuthError);

        await db_api.read_message.run(req.params.id);

        res.render('ajax/inbox-message', { item: message });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_outbox_message = async function(req, res, next) {
    try {
        const message = await db_api.get_message_by_id.get(req.params.id);
        
        if (message.sender_id != req.session.user_id) throw new Error(codes.badRequest, errors.messageAuthError);

        res.render('ajax/outbox-message', { item: message });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_make_response = async function(req, res, next) {
    try {
        let message = await db_api.get_message_by_id.get(req.params.id);
        
        if (message.receiver_id != req.session.user_id) throw new Error(codes.badRequest, errors.messageAuthError);
        
        message.quote = message.sender_name + ' said:\n"' + message.content + '"\n\n';

        res.render('ajax/make-response', { item: message });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_scoreboards = async function(req, res, next) {
    try {
        const planetsScoreboard = await helper.getPlanetsScoreboard();
        const miliScoreboard = await helper.getMilitaryScoreboard();

        res.render('ajax/scoreboards', { planets: planetsScoreboard, militaryPower: miliScoreboard, userID: req.session.user_id });
        
    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_index = function(req, res, next) {
    try {
        fs.readFile('views/ajax/index.html', function(err, data) {
            if (err) return errors.resError(res, new Error(codes.serverError, errors.viewError));
    
            res.send(data.toString());
        });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_index_classes = async function(req, res, next) {
    try {
        const classes = await db_api.get_classes.all();

        const antiShield = classes.filter(obj => obj.type == 'Anti Shield');
        const spaceFleet = classes.filter(obj => obj.type == 'Space Fleet');
        const groundForces = classes.filter(obj => obj.type == 'Ground Forces');
        
        res.render('ajax/classes', { types: [antiShield, spaceFleet, groundForces] });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_index_resources = async function(req, res, next) {
    try {
        const resources = await db_api.get_resources.all();
        
        res.render('ajax/resources', { items: resources });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_friend_list = async function(req, res, next) {
    try {
        const outgoingRequests = await db_api.get_user_outgoing_requests.all(req.session.user_id);
            
        const pendingRequests = await db_api.get_user_pending_requests.all(req.session.user_id);

        const friendlist = await db_api.get_user_friendlist.all(req.session.user_id, req.session.user_id);

        res.render('ajax/friendlist', {
            pendingOutRequests: outgoingRequests,
            pendingInRequests: pendingRequests,
            friends: friendlist
        });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_friends_add_by_nickname = async function(req, res, next) {
    try {
        // check if user exists
        const nicknameCheck = await db_api.check_other_nickname.get(req.params.name, req.session.user_id);
        if (!nicknameCheck) throw new Error(codes.badRequest, errors.userNotFoundError);

        const friendID = nicknameCheck.id;

        // check if a request wasnt sent before
        const friendRequestCheck = await db_api.check_friend_request.get(req.session.user_id, friendID, req.session.user_id, friendID);
        if (friendRequestCheck) throw new Error(codes.badRequest, errors.requestAlreadySent);

        // check if they are friends already
        const friendsCheck = await db_api.check_friends.get(req.session.user_id, friendID, req.session.user_id, friendID);
        if (friendsCheck) throw new Error(codes.badRequest, errors.alreadyFriends);

        // add friend
        await db_api.add_friend.run(req.session.user_id, req.params.name);

        res.status(codes.ok).json({
            msg: 'OK'
        });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_friend_request_cancel = async function(req, res, next) {
    try {
        await db_api.delete_request.run(req.session.user_id, req.params.id);

        res.status(codes.ok).json({
            msg: 'OK'
        });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_friend_request_decline = async function(req, res, next) {
    try {
        await db_api.delete_request.run(req.params.id, req.session.user_id);

        res.status(codes.ok).json({
            msg: 'OK'
        });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_friend_request_accept = async function(req, res, next) {
    try {
        await db_api.accept_request.run(req.params.id, req.session.user_id);

        res.status(codes.ok).json({
            msg: 'OK'
        });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_remove_friend = async function(req, res, next) {
    try {
        await db_api.delete_friend.run(req.params.id, req.session.user_id, req.params.id, req.session.user_id);

        res.status(codes.ok).json({
            msg: 'OK'
        });

    } catch(e) {
        return errors.resError(res, e);
    }
}

exports.get_planet_conquest = async function(req, res, next) {
    try {
        // update all user planets
        await helper.updateUserPlanetArmies(req.session.user_id);
        
        const planets = await db_api.get_user_planets.all(req.session.user_id);

        const requiredMilitaryPower = await db_api.get_planet_required_military_power.get(req.session.visited_planet_id);

        res.render('ajax/conquest', { planets: planets, requiredMilitaryPower: requiredMilitaryPower });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_conquest_planet_units = async function(req, res, next) {
    try {
        const antiShield = await db_api.get_planet_units_by_type.all(req.params.id, 'Anti Shield');    
        const space = await db_api.get_planet_units_by_type.all(req.params.id, 'Space Fleet');
        const ground = await db_api.get_planet_units_by_type.all(req.params.id, 'Ground Forces');
        
        res.render('ajax/conquest-units', { antiShield: antiShield, space: space, ground: ground });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.post_planet_conquest = async function(req, res, next) {
    try {        
        // make sure that the planet does not belong to, or being conquered by any empire.
        const armyCheck = await db_api.check_planet_army.get(req.session.visited_planet_id);
        if (armyCheck.has_army) throw new Error(codes.badRequest, errors.planetHasArmyError);

        // make sure the planet is not a home planet (you cannon conquer a home planet).
        const homePlanetCheck = await db_api.check_home_planet.get(req.session.visited_planet_id);
        if (homePlanetCheck) throw new Error(codes.badRequest, errors.homePlanetConquestError);

        let units = [];
        let maxDistance = 0;
        let militaryPower = 0;
    
        // check if the units are valid and collect data about the new army and travel distance.
        for (let i in req.body.units) {
            let unit = req.body.units[i];

            if (unit.amount == 0) throw new Error(codes.badRequest, errors.conquestZeroUnitsError);

            let unitData = await db_api.get_conquest_unit.get(unit.amount, req.session.user_id, unit.unitID);
            if (!unitData || !unitData.valid) throw new Error(codes.badRequest, errors.invalidActionError);

            unitData.deployed = unit.amount;
            units.push(unitData);
        
            // get maximal arrival distance
            const distance = await helper.getPlanetsDistance(req.session.visited_planet_id, unitData.planet_id);
            if (distance > maxDistance) maxDistance = distance;

            militaryPower += unitData.attack;
            militaryPower += unitData.defense;
        }

        let militaryPowerCheck = await db_api.check_military_power.get(militaryPower, req.session.visited_planet_id);
        if (!militaryPowerCheck.military_power_suffies) throw new Error(codes.badRequest, errors.invalidActionError);

        // calculate conquest delay.
        let delay = 5; //formulas.conquestDelay(maxDistance);

        // check if planet has population
        const populationCheck = await db_api.check_planet_population.get(req.session.visited_planet_id);

        if (populationCheck.populated) {
            // looting process
            const lootData = await db_api.get_planet_loot_data.get(req.session.visited_planet_id);
            
            // calculate loot amounts
            const resourceLoot1 = formulas.resourceLootAmount(lootData.development_level);  // (its random)
            const resourceLoot2 = formulas.resourceLootAmount(lootData.development_level);
            const gcLoot = formulas.gcLootAmount(lootData.development_level); 

            // add loot
            await helper.addTransfer(lootData.resource_1, resourceLoot1, req.session.user_id, delay);
            await helper.addTransfer(lootData.resource_2, resourceLoot2, req.session.user_id, delay);
            await helper.addTransfer('gc', gcLoot, req.session.user_id, delay);    

            // if the planet is ruled by an empire and not by locals then steal resources and gc from the empire.
            if (lootData.empire_id != 2) {
                await helper.addTransfer(lootData.resource_1, -1 * resourceLoot1, lootData.user_id, delay);
                await helper.addTransfer(lootData.resource_2, -1 * resourceLoot2, lootData.user_id, delay);
                await helper.addTransfer('gc', -1 * gcLoot, lootData.user_id, delay); 
            }          
            
            // notify to the attacker that the conquest succeeded.
            await notifications.planetConqueredAndLootedAttacker(
                req.session.user_id,
                delay,
                req.session.visited_planet_name,
                lootData.resource_1,
                lootData.resource_2,
                resourceLoot1,
                resourceLoot2,
                gcLoot
            );

            if (lootData.empire_id != 2) {  // if there is a defender (a non local population).
                // notify to the defender.
                await notifications.planetConqueredAndLootedDefender(
                    lootData.user_id,
                    delay,
                    req.session.visited_planet_name,
                    req.session.nickname,
                    req.session.empire_name,
                    lootData.resource_1,
                    lootData.resource_2,
                    resourceLoot1,
                    resourceLoot2,
                    gcLoot
                );           
            }              

        } else {
            await notifications.planetConqueredAttacker(
                req.session.user_id,
                delay,
                req.session.visited_planet_name
            );
        }     
           
        for (let i in units) {   
            let unit = units[i];

            await db_api.reduce_combat_and_combat_support.run(unit.combat, unit.combat_support, unit.army_id);  // reduce combat and combat support from the original army
            await db_api.add_combat_and_combat_support.run(unit.combat, unit.combat_support, req.session.visited_planet_id);  // add to new army               
            
            // moving the units to the planet army
            const unitInArmyCheck = await db_api.check_unit_in_army.get(unit.unit_type_id, req.session.visited_planet_id);

            if (unitInArmyCheck) {  // if the unit of the same type is in the army.
                if (unit.deployed == unit.amount) {
                    await db_api.remove_units_from_army.run(unit.unit_id);  // if all of the units are deployed, delete them from their old army.
                } else {
                    await db_api.reduce_army_units.run(unit.deployed, unit.unit_id);  // if only some of the units are deployed, reduce units from the old army.
                }
                await db_api.add_existing_units_to_army.run(unit.deployed, unit.unit_type_id, req.session.visited_planet_id);  // add units to the new army.
            } else {
                if (unit.deployed == unit.amount) {  
                    await db_api.move_all_units_to_planet_army.run(req.session.visited_planet_id, unit.unit_id);  // if all units are deployed move them to the new army.                  
                }  else {
                    await db_api.reduce_army_units.run(unit.deployed, unit.unit_id);  // if only some of the units are deployed, reduce units from the old army.
                    await db_api.add_new_units_to_army.run(unit.deployed, unit.unit_type_id, req.session.visited_planet_id);  // add the units to the new army.
                }
            }
        } 
        
        await db_api.conquer_planet.run(req.session.user_id, helper.formatDate(helper.delayedTime(delay)), req.session.visited_planet_id);
        await db_api.change_conquered_population_status.run('undecided', req.session.visited_planet_id);
        await db_api.remove_planet_waiting_units.run(req.session.visited_planet_id);

        res.status(codes.ok).json({
            msg: 'OK'
        });

    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_notifications = async function(req, res, next) {
    try {
        let userNotifications = await db_api.get_user_notifications.all(req.session.user_id);
        await db_api.view_user_notifications.run(req.session.user_id);
        
        res.render('ajax/notifications', { item: userNotifications });

    } catch (e) {
        return errors.resError(res, e);
    }
}