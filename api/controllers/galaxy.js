const fs = require('fs');
const db_api = require('../database/db-api');
const helper = require('../helpers/helper');
const errors = require('../helpers/errors');
const codes = require('../helpers/status-codes');
const Error = errors.Error;


exports.get_galaxy = function(req, res, next) {
    fs.readFile('views/galaxy.html', function(err, data) {
        if (err) return errors.resError(res, new Error(codes.unauthorized, errors.viewError));
        
        res.send(data.toString());
    });
}