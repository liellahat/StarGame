const fs = require('fs');
const db_api = require('../database/db-api');
const helper = require('../helpers/helper');
const formulas = require('../helpers/formulas');
const errors = require('../helpers/errors');
const codes = require('../helpers/status-codes');
const Error = errors.Error;


exports.get_planet =  async function(req, res, next) {
    try {
        await helper.updatePlanetArmy(req.params.id);     
        const planet = await db_api.get_planet_by_id.get(req.params.id);
        const resources = await db_api.get_user_resources.get(req.session.user_id);
        let planetsList = await db_api.get_user_planets.all(req.session.user_id);      

        // alerts
        const messagesAlert = await db_api.get_unread_messages.get(req.session.user_id) != undefined;
        const friendListAlert = await db_api.get_user_pending_requests.get(req.session.user_id) != undefined;
        const notificationsAlert = await db_api.get_user_unviewed_notifications.get(req.session.user_id) != undefined;

        if (planet.owner_id == req.session.user_id && !planet.being_conquered) {  // if it's the user's planet
            req.session.planet_id = req.params.id;
            req.session.planet_name = planet.name;

            req.session.visited_planet_id = null;
            req.session.visited_planet_name = null;

            planetsList = planetsList.filter(function(item) {  // remove the current session planet id from planets dropdown list (because it will appear anyway).
                return item.id != req.session.planet_id;
            }); 

            console.log(planet);

            let populationChoice = planet.conquered_status == 'undecided';  // whether the user is presented with the population choice (kill, enslave, free)

            res.render('owner-planet', {
                item: planet,
                populationChoice: populationChoice,
                planetsList: planetsList,
                planetName: req.session.planet_name,
                planetID: req.session.planet_id,
                resources: resources,
                userID: req.session.user_id,
                friendListAlert: friendListAlert,
                messagesAlert: messagesAlert,
                notificationsAlert: notificationsAlert
            });     
        } else {     
            req.session.visited_planet_id = req.params.id;
            req.session.visited_planet_name = planet.name;

            planetsList = planetsList.filter(function(item) {
                return item.id != req.session.planet_id;
            }); 

            res.render('other-planet', { 
                item: planet,
                planetName: req.session.planet_name,
                planetID: req.session.planet_id,
                resources: resources,
                userID: req.session.user_id,
                friendListAlert: friendListAlert,
                messagesAlert: messagesAlert,
                notificationsAlert: notificationsAlert,
                planetsList: planetsList
            });     
        }     
    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_home_planet = async function(req, res, next) {
    try {
        const planet = await db_api.get_home_planet.get(req.session.user_id);
        res.redirect('/planet/' + planet.id);
        
        req.session.planet_id = planet.id;
        req.session.planet_name = planet.name;

    } catch(e) {
        return errors.resError(res, e);
    }
}

exports.get_current_planet = async function(req, res, next) {
    res.redirect('/planet/' + req.session.planet_id);
}

exports.get_population_peace = async function(req, res, next) {
    try {
        await db_api.make_population_peace.run(req.session.planet_id);
        res.redirect('/planet/' + req.session.planet_id);
        
    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_population_slavery = async function(req, res, next) {
    try {
        await db_api.enslave_population.run(req.session.planet_id);
        res.redirect('/planet/' + req.session.planet_id);
        
    } catch (e) {
        return errors.resError(res, e);
    }
}

exports.get_population_extermination = async function(req, res, next) {
    try {
        const populationData = await db_api.get_population_size.get(req.session.user_id);
        const delay = 600;// formulas.exterminationDelay(populationData.size);

        await db_api.exterminate_population.run(helper.formatDate(helper.delayedTime(delay)), req.session.user_id, req.session.planet_id);

        res.redirect('/planet/' + req.session.planet_id);
        
    } catch (e) {
        return errors.resError(res, e);
    }
}