const fs = require('fs');
const errors = require('../helpers/errors');
const codes = require('../helpers/status-codes');
const Error = errors.Error;

exports.get_race = function(req, res, next) {
    fs.readFile('views/race.html', function(err, data) {
        if (err) return errors.resError(res, new Error(codes.serverError, errors.viewError));
        
        res.send(data.toString());
    });
}