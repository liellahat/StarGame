const fs = require('fs');
const bcrypt = require('bcrypt');
const db_api = require('../database/db-api');
const helper = require('../helpers/helper');
const mailer = require('../helpers/mailer');
const guid = require('guid');
const errors = require('../helpers/errors');
const codes = require('../helpers/status-codes');
const Error = errors.Error;  // shortcut


exports.get_signup = function(req, res, next) {
    req.session.user_id = null;
    req.session.planet_id = null;
    req.session.visited_planet_id = null;

    if (req.session.error) {
        res.render('signup', { error: req.session.error });
    } else {
        res.render('signup');
    }
    
    req.session.error = null;
}

exports.post_signup = async function(req, res, next) {
    try {   
        // checks
        helper.outOfLimits(req.body, 'username', 3, 16, new Error(codes.badRequest, errors.invalidUsernameLengthError));
        helper.outOfLimits(req.body, 'nickname', 3, 16, new Error(codes.badRequest, errors.invalidNicknameLengthError));
        helper.outOfLimits(req.body, 'password', 4, 256, new Error(codes.badRequest, errors.invalidPasswordLengthError));
        
        if (req.body.password != req.body.passwordConfirm) throw new Error(codes.badRequest, errors.passwordConfirmError);

        if (!helper.isEmail(req.body.email)) throw new Error(codes.badRequest, errors.invalidEmailError);
        
        const username = req.body.username.trim();
        const password = req.body.password.trim();
        const nickname = req.body.nickname.trim();
        const email = req.body.email.trim();
        const userGuid = guid.create();
        
        // checking if username taken
        const usernameCheck = await db_api.check_username.get(username);  
        if (usernameCheck) throw new Error(codes.badRequest, errors.usernameTakenError);        

        // checking if nickname taken
        const nicknameCheck = await db_api.check_nickname.get(nickname);
        if (nicknameCheck) throw new Error(codes.badRequest, errors.nicknameTakenError);

        // checking if email taken
        const emailCheck = await db_api.check_email.get(email);
        if (emailCheck) throw new Error(codes.badRequest, errors.emailTakenError);
        
        // hashing password
        bcrypt.hash(password, 10, async function (err, hash) {
            try {  
                if (err) throw new Error(codes.serverError, errors.hashError);   

                // user accepted  
                res.redirect('email-confirm');   
           
                await mailer.send(email, 'Email confirmation', 'Press this link to confirm your email:\nhttp://localhost:9292/user/email-confirm/' + userGuid.value);

                await db_api.insert_user.run(username, nickname, hash, userGuid.value, email);

            } catch (e) {       
                return errors.sessionError(req, res, e, 'signup');
            }

        });

    } catch (e) {   
        return errors.sessionError(req, res, e, 'signup');
    }
}

exports.get_login = function(req, res, next) {
    helper.logout(req);

    if (req.session.error) {
        res.render('login', { error: req.session.error });
    } else {
        res.render('login');
    }

    req.session.error = null;
}

exports.post_login = async function(req, res, next) {
    try {        
        const username = req.body.username.trim();
        const password = req.body.password.trim();

        // getting user by username
        const user = await db_api.get_user_by_username.get(username);
        if (!user) throw new Error(codes.badRequest, errors.loginError);

        // comparing username and password
        bcrypt.compare(password, user.password_hash, async function(err, result) {
            try {
                if (err) throw new Error(codes.serverError, errors.hashError);  // if hashing failed from some reason (internal).    
                if (!result) throw new Error(codes.badRequest, errors.loginError);  // if password doesnt match.
    
                // user accepted
                await helper.login(req, user.id); 
    
                // if user doesn't have an empire
                if (!user.empire_id) return res.redirect('/race');

                res.redirect('/planet/home');

            } catch (e) {
                return errors.sessionError(req, res, e, 'login');
            }             
        });
        
    } catch (e) {
        return errors.sessionError(req, res, e, 'login');
    }
}

exports.get_logout = function(req, res, next) {
    helper.logout(req);
    res.redirect('/user/login');
}

exports.get_email_confirm = function(req, res, next) {
    fs.readFile('views/pre-email-confirm.html', function(err, data) {
        if (err) return errors.resError(res, new Error(codes.serverError, errors.viewError));
        
        res.send(data.toString());
    });
    
}

exports.get_email_confirm_guid = async function(req, res, next) {
    await db_api.verify_user.run(req.params.guid, req.session.user_id);
    fs.readFile('views/past-email-confirm.html', function(err, data) {
        if (err) return errors.resError(res, new Error(codes.serverError, errors.viewError));
        
        res.send(data.toString());
    });
}