// libraries
const express = require('express');
const path = require('path');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const validator = require('express-validator');
const session = require('express-session');
const SQLiteStore = require('connect-sqlite3')(session);
const hbs = require('./hbs');
const mailer = require('./api/helpers/mailer');

// api
const db_api = require('./api/database/db-api');
const loginRoutes = require('./api/routes/user');
const planetRoutes = require('./api/routes/planet');
const solarSystemRoutes = require('./api/routes/solar-system');
const galaxyRoutes = require('./api/routes/galaxy');
const raceRoutes = require('./api/routes/race');
const ajaxRoutes = require('./api/routes/ajax');
const loops = require('./api/loops');

const app = express();

// start db
db_api.build_db();

// start loop
loops.start();

// start mailer
mailer.setup();

// set view engine
app.engine('hbs', hbs.engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// configurations
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(validator());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    store: new SQLiteStore,
    secret: process.env.SECRET,
    saveUninitialized: false,
    resave: false,
    cookie: { maxAge: 7 * 24 * 60 * 60 * 1000 }  // 1 week
}));
app.use(function(req, res, next) {  // CORS error prevention
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Acess-Control-Allow-Headers', '*');

    if (req.method == 'OPTIONS') {
        res.header('Acess-Control-Allow-Methods', 'GET, POST');
        return res.status(200).json({});
    }

    next();
});

// routes
app.use('/', function(req, res, next) {
    if (req.url == '/') {
        req.url = '/solar-system';
    }

    next();
});
app.use('/user', loginRoutes);
app.use('/planet', planetRoutes);
app.use('/solar-system', solarSystemRoutes);
app.use('/galaxy', galaxyRoutes);
app.use('/race', raceRoutes);
app.use('/ajax', ajaxRoutes);

// 404
app.use(function(req, res, next) {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

// catch
app.use(function(error, req, res, next) {
    res.status(error.status || 500).json({
        error: {
            message: error.message
        }
    });
    console.log(error.message);
});

module.exports = app;