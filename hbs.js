const hbs = require('express-handlebars');

// a helper function because hbs is weird
function formatNumber(num, acc) {         
    if (typeof(num) == 'string') {
        num = parseInt(num);
    }

    if (num > 10000000000000) return Math.round(num / 1000000000000 * 10) / 10 + 'Q';
    if (num > 10000000000) return Math.round(num /1000000000 * 10) / 10 + 'B';
    if (num > 10000000) return Math.round(num / 1000000 * 10) / 10 + 'M'; 
    if (num > 10000) return Math.round(num / 1000 * 10) / 10 + 'K';
    
    return Math.round(num * 10) / 10;
}

module.exports = hbs.create({
    helpers: {
        formatNumber: formatNumber,
        toUpperCase: function(str) {
            return str.toUpperCase();
        },
        toLowerCase: function(str) {
            return str.toLowerCase
        },
        formatMul: function(a, b) {
            return formatNumber(a * b);
        },
        formatPercentage: function(a, b) {
            return formatNumber(100 * a / b);
        }, 
        tillLast: function(arr) {
            return arr.slice(0, arr.length - 1);
        },
        last: function(arr) {
            return arr[arr.length - 1];
        },
        attribute: function(arr, attr) {
            return arr[attr];
        },
        getUnitStep: function(unitAmount) {
            if (unitAmount > 20) return parseInt(unitAmount / 20);
            else return 1;
        },
        equals: function(a, b) {
            return a == b;
        },
        notEquals: function(a, b) {
            return a != b;
        },
        not: function(a) {
            return !a;
        },
        and: function(a, b) {
            return a && b;
        }
    }
});