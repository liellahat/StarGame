let militaryShop = (function() {
	let open = false;
	let batch;
	let unitAttack;
	let unitDefense;
	let unitResource1;
	let unitResource2;
	let resource1Pic;
	let resource2Pic;
	let unitCombat;
	let unitCombatSupport;
	let unitGcph;

	function start() {
        popUps = [];

        if (open) close();
        else load();
        
        open = !open;
	}

	function load() {
        general.handlePopUps('military-shop');
		general.load('/ajax/military-shop', 'military-shop');
	}

    function close() {
        popUps.pop();
        general.closePopUps();     
    }

	$(document).on('click', '#close-military-shop', close);

	// categories
	$(document).on('click', '#military-shop-all', function() {
		general.load('/ajax/military-shop', 'military-shop');
	});

	$(document).on('click', '#military-shop-anti-shield', function() {
		general.load('/ajax/military-shop/anti-shield', 'military-shop');
	});

	$(document).on('click', '#military-shop-space-fleet', function() {
		general.load('/ajax/military-shop/space-fleet', 'military-shop');
	});

	$(document).on('click', '#military-shop-ground-forces', function() {
		general.load('/ajax/military-shop/ground-forces', 'military-shop');
	});

	// extended item
	$(document).on('click', '.unit', function() {
        general.handlePopUps('military-shop-item');
		general.load('/ajax/military-shop/item/' + this.id, 'military-shop', function() {
			batch = parseInt(document.getElementById('extended-item-batch').innerHTML);
			unitAttack = parseFloat(document.getElementById('extended-item-unit-attack').innerHTML);
			unitDefense = parseFloat(document.getElementById('extended-item-unit-defense').innerHTML);
			unitResource1 = parseFloat(document.getElementById('extended-item-unit-resource-1').innerHTML);
			unitResource2 = parseFloat(document.getElementById('extended-item-unit-resource-2').innerHTML);
			resource1Pic = document.getElementById('extended-item-resource-1').innerHTML.split('>')[0] + '>';
			resource2Pic = document.getElementById('extended-item-resource-2').innerHTML.split('>')[0] + '>';
			unitCombat = parseInt(document.getElementById('extended-item-unit-combat').innerHTML);
			unitCombatSupport = parseInt(document.getElementById('extended-item-unit-combat-support').innerHTML);
			unitGcph = parseFloat(document.getElementById('extended-item-unit-gcph').innerHTML);
		});
	});

	function shopCostUpdate(amount) {
		document.getElementById('extended-item-amount').value = amount;
		document.getElementById('extended-item-attack').innerHTML = general.formatNumber(amount * unitAttack, 0) + '<br/>ATK';
		document.getElementById('extended-item-defense').innerHTML = general.formatNumber(amount * unitDefense, 0) + '<br/>DEF';
		document.getElementById('extended-item-resource-1').innerHTML = resource1Pic + ' x ' + general.formatNumber(amount * unitResource1);
		document.getElementById('extended-item-resource-2').innerHTML = resource2Pic + ' x ' + general.formatNumber(amount * unitResource2);
		document.getElementById('extended-item-combat').innerHTML = '<img src="/images/icons/soldier.png"> x ' + general.formatNumber(amount * unitCombat);
		document.getElementById('extended-item-combat-support').innerHTML = '<img src="/images/icons/support.png"> x ' + general.formatNumber(amount * unitCombatSupport);
		document.getElementById('extended-item-gcph').innerHTML = '<img src="/images/icons/gc.png"> per hour x ' + general.formatNumber(amount * unitGcph);
	}

	function shopCostUpdateZero() {
		document.getElementById('extended-item-amount').value = 0;
		document.getElementById('extended-item-attack').innerHTML = '0<br/>ATK';
		document.getElementById('extended-item-defense').innerHTML = '0<br/>DEF';
		document.getElementById('extended-item-resource-1').innerHTML = resource1Pic + ' x 0';
		document.getElementById('extended-item-resource-2').innerHTML = resource2Pic + ' x 0';
		document.getElementById('extended-item-combat').innerHTML = '<img src="/images/icons/soldier.png"> x 0';
		document.getElementById('extended-item-combat-support').innerHTML = '<img src="/images/icons/support.png"> x 0';
		document.getElementById('extended-item-gcph').innerHTML = '<img src="/images/icons/gc.png"> per hour x 0';
	}


	$(document).on('click', '#close-extended-item', function() {
		popUps.pop();
		
		if (popUps[popUps.length - 1] == 'military-shop') {
			load();
		}
	});

	$(document).on('click', '#extended-item-plus', function() {
		let amount = parseInt(document.getElementById('extended-item-amount').value) + batch;

		if (!isNaN(amount)) {
			shopCostUpdate(amount);
		}
	});

	$(document).on('click', '#extended-item-minus', function() {
		let amount = parseInt(document.getElementById('extended-item-amount').value) - batch;

		if (amount > 0) {
			shopCostUpdate(amount);
		} else if (!isNaN(amount)) {
			shopCostUpdateZero();
		}
	});

	$(document).on('change', '#extended-item-amount', function() {
		let amount = parseInt(document.getElementById('extended-item-amount').value);

		if (amount > 0) {
			shopCostUpdate(amount);
		} else if (!isNaN(amount)) {
			shopCostUpdateZero();
		}
	});

	$(document).on('click', '#extended-item-buy', function() {
		let msgDiv = document.getElementById('extended-item-msg');

		$.ajax({
			url: '/ajax/military-shop/buy',
			dataType: 'json',
			type: 'post',
			contentType: 'application/json',
			data: JSON.stringify({
				id: document.getElementsByClassName('extended-item')[0].id, 
				amount: parseInt(document.getElementById('extended-item-amount').value) 
			}),
			processData: false,
			success: function(data) {
				topDiv.updateResources(data.costData);
				msgDiv.style.background = '#28A853';
				msgDiv.innerHTML = 'Purchase complete.';
				$(msgDiv).fadeOut();
				$(msgDiv).fadeIn();
			},
			error: function(err) {
				msgDiv.style.background = 'rgb(248, 79, 79)';
				msgDiv.innerHTML = err.responseJSON.error;
				$(msgDiv).fadeOut();
				$(msgDiv).fadeIn();
			}
		});
	});

	return {
		load: load,
		start: start
	};
})();