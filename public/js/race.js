let name;
let lore;
let survivability = 0;
let breedRate = 0;
let intellegence = 0;
let combatInstincts = 0;
let pointsCount = 25;
let captchaToken;

function loadStats() {
    raceName = document.getElementById('race-name').value;
    empireName = document.getElementById('empire-name').value;
    lore = document.getElementById('lore').value;

    $.ajax({
        url: '/ajax/race/name-check',
        dataType: 'json',
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify({
            raceName: raceName,
            empireName: empireName,
            lore: lore
        }),
        processData: false,
        success: function(data) {
            $('#submit').css({ 'display': 'block' });

            $('#error')
                .css({
                    'display': 'none',
                    'left': '13%',
                    'top': '77%',
                    'background': '#28A853'
                })
                .html('Make sure you use all the points. You won\'t be abe to change these attributes later on.')
                .fadeOut()
                .fadeIn();

            general.load('/ajax/race/stats', 'content-div');   
        }, 
        error: function(err) {
            $('#error')
                .html(err.responseJSON.error)
                .fadeOut()
                .fadeIn();          
        }
    });
}

function dataCallback(token) {    
    captchaToken = token;
    $.ajax({
        url: '/ajax/race',
        dataType: 'json',
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify({
            raceName: raceName,
            empireName: empireName,
            lore: lore,
            survivability: survivability,
            breedRate: breedRate,
            intellegence: intellegence,
            combatInstincts: combatInstincts,
            captcha: captchaToken
        }),
        processData: false,
        success: function(data) {
            window.location.href = '/planet/home';  
        },
        error: function(err) {
            $('#error')
                .css({
                    'background': 'rgb(248, 79, 79)'
                })
                .html(err.responseJSON.error)
                .fadeOut()
                .fadeIn();

            // changing onclick event incase of re-submitting the form (data-callback works only once).
            $('#submit').attr('onclick', submit);               
        }
    });
}

$(document).on('click', '.plus', function() {
    let n = this.id.split('-')[1];

    if (pointsCount > 0) {
        if (n == 1) {
            if (survivability >= 0 && survivability <= 9) {
                survivability++;
                document.getElementById('p-1-' + survivability).style.background = '#000000';
                pointsCount--;
                document.getElementById('points-count').innerHTML = pointsCount + ' POINTS';
            }
        } else if (n == 2) {
            if (breedRate >= 0 && breedRate <= 9) {
                breedRate++;
                document.getElementById('p-2-' + breedRate).style.background = '#000000'; 
                pointsCount--;
                document.getElementById('points-count').innerHTML = pointsCount + ' POINTS';
            }
        } else if (n == 3) {
            if (intellegence >= 0 && intellegence <= 9) {
                intellegence++;
                document.getElementById('p-3-' + intellegence).style.background = '#000000';
                pointsCount--;
                document.getElementById('points-count').innerHTML = pointsCount + ' POINTS';
            }
        } else if (n == 4) {
            if (combatInstincts >= 0 && combatInstincts <= 9) {
                combatInstincts++;
                document.getElementById('p-4-' + combatInstincts).style.background = '#000000';
                pointsCount--;
                document.getElementById('points-count').innerHTML = pointsCount + ' POINTS';
            }
        }    
    }
});

$(document).on('click', '.minus', function() {
    let n = this.id.split('-')[1];

    if (n == 1) {
        if (survivability >= 1 && survivability <= 10) {
            document.getElementById('p-1-' + survivability).style.background = '#ffffff';
            survivability--;
            pointsCount++;
            document.getElementById('points-count').innerHTML = pointsCount + ' POINTS';
        }
    } else if (n == 2) {
        if (breedRate >= 1 && breedRate <= 10) {
            document.getElementById('p-2-' + breedRate).style.background = '#ffffff'; 
            breedRate--;
            pointsCount++;
            document.getElementById('points-count').innerHTML = pointsCount + ' POINTS';
        }
    } else if (n == 3) {
        if (intellegence >= 1 && intellegence <= 10) {
            document.getElementById('p-3-' + intellegence).style.background = '#ffffff';
            intellegence--;
            pointsCount++;
            document.getElementById('points-count').innerHTML = pointsCount + ' POINTS';
        }
    } else if (n == 4) {
        if (combatInstincts >= 1 && combatInstincts <= 10) {
            document.getElementById('p-4-' + combatInstincts).style.background = '#ffffff';
            combatInstincts--;
            pointsCount++;
            document.getElementById('points-count').innerHTML = pointsCount + ' POINTS';
        }
    }  
});