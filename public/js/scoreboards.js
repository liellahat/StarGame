let scoreboards = (function() {
    let open = false;

    function start() {
        popUps = [];

        if (open) close();
        else load();
        
        open = !open;
    }

    function load() {
		general.handlePopUps('scoreboards');
        general.load('/ajax/scoreboards', 'scoreboards'); 
    }

    function close() {
        popUps.pop();
        general.closePopUps();     
    }

    $(document).on('click', '#close-scoreboards', close);

    $(document).on('click', '.scoreboards-line', function() {
        if (yourID == this.id) {
            selfProfile.load();
        } else {
            profile.load(this.id);
        }
    });

    return {
        load: load,
        start: start
    }
})();