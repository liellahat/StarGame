//create and set renderer
var renderer = new THREE.WebGLRenderer({canvas: document.getElementById('canvas'), antialias: true});
renderer.setClearColor(0x0);
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);

//create and set camera
var camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 0.1, 3000);  // fov, aspect ratio, near, far
camera.position.set(0,0,1);

var scene = new THREE.Scene();
document.body.appendChild(renderer.domElement);

//create lights
var light = new THREE.AmbientLight(0xffffff, 0.6);	
var light1 = new THREE.PointLight(0xffffff, 1);	
var light2 = new THREE.PointLight(0xffffff, 0.3);	
light1.position.set( 700, 300, -1500 );
scene.add(light);
scene.add(light1);
scene.add(light2);

//create and position the mesh (inspected star)
var mesh = new THREE.Mesh(
	new THREE.SphereGeometry(90, 30, 30),
	new THREE.MeshPhongMaterial({
		map: new THREE.TextureLoader().load(document.getElementById('planet-texture').innerHTML)
	})
);
mesh.position.set(-100, -17, -375);
scene.add(mesh);

// create background stars
createBackground();

animate();
function animate() {
	mesh.rotation.y += 0.001;	
	requestAnimationFrame(animate);
	render();
}

function render() {	
	renderer.render(scene, camera);
}

function createBackground(){
	var x = -1700;
    var y = -1500;
    
	for (var j = 0; j <= 30; j++) {
		var geometry = new THREE.PlaneGeometry(720, 720);
		var material = new THREE.MeshBasicMaterial({
			map: new THREE.TextureLoader().load("/images/background2.jpg"), 
			side: THREE.DoubleSide
		});
		var plane = new THREE.Mesh(geometry, material);
		x += 720;
		if (j % 7 == 0){
			x = -1700;
			y += 720;
		}
		plane.rotation.z = (((j%5)/2) * Math.PI);
		plane.position.set(x, y, -3000);
		scene.add(plane);
	}	
}
