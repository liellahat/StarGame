let population = (function() {
	let open = false;

	let capacity;
	let size;
	let combatReady;
	let combatSupportReady;
	let state;
	let stateDiv;
	let gcPayers;
	let resource1Workers = 0;
	let resource2Workers = 0;
	let gcMeter;
	let resource1Meter;
	let resource2Meter;
	let popPerHour1;
	let popPerHour2;
	let popPicPath1;
	let popPicPath2;
	let mine1Built;
	let mine2Built;

	function start() {
        popUps = [];

        if (open) close();
        else load();
        
        open = !open;
    }

	function load() {
		general.handlePopUps('population');
		general.load('/ajax/population', 'population', function() {

			size = document.getElementById('population-size-invisible').innerHTML;
			stateDiv = document.getElementById('population-state');
			state = document.getElementById('population-state-invisible').innerHTML;
			gcMeter = document.getElementById('population-gc-payers-marked');
			mine1Built = document.getElementById('population-resource-1-workers-number').innerHTML != 'No mine.';
			mine2Built = document.getElementById('population-resource-2-workers-number').innerHTML != 'No mine.';
			gcPayers = parseInt(document.getElementById('population-gc-payers-number-invisible').innerHTML);


			if (mine1Built) {
				popPerHour1 = document.getElementById('population-resource-1-per-hour-invisible').innerHTML;
				resource1Meter = document.getElementById('population-resource-1-workers-marked');
				popPicPath1 = document.getElementById('population-resource-1-per-hour').innerHTML.split(' per hour')[0];
				resource1Workers = parseInt(document.getElementById('population-resource-1-workers-number-invisible').innerHTML);
			}

			if (mine2Built) {
				popPerHour2 = document.getElementById('population-resource-2-per-hour-invisible').innerHTML;
				resource2Meter = document.getElementById('population-resource-2-workers-marked'); 
				popPicPath2 = document.getElementById('population-resource-2-per-hour').innerHTML.split(' per hour')[0];	
				resource2Workers = parseInt(document.getElementById('population-resource-2-workers-number-invisible').innerHTML);
			}	
	
			// state color
			if (state > 70) {
				stateDiv.style.borderColor = 'green';
			} else if (state > 50) {
				stateDiv.style.borderColor = '#cccc00';
			} else if (state > 30) {
				stateDiv.style.borderColor = '#ff9900';
			} else {
				stateDiv.style.borderColor = 'red';
			}		
	
			gcMeter.style.width = (100 * gcPayers / size).toString() + '%';

			if (mine1Built) resource1Meter.style.width = (100 * resource1Workers / size).toString() + '%';
			if (mine2Built) resource2Meter.style.width = (100 * resource2Workers / size).toString() + '%';	
		});
	}

	function close() {
        popUps.pop();
		general.closePopUps();  
	}
	
	function updateMeters() {
		gcPayers = size - resource1Workers - resource2Workers;
	
		document.getElementById('population-gc-payers-number').innerHTML = general.formatNumber(gcPayers);	
		gcMeter.style.width = (100 * gcPayers / size).toString() + '%';

		if (mine1Built) {
			document.getElementById('population-resource-1-workers-number').innerHTML = general.formatNumber(resource1Workers);
			document.getElementById('population-resource-1-per-hour').innerHTML = popPicPath1 + ' per hour x ' + general.formatNumber(resource1Workers * popPerHour1);
			resource1Meter.style.width = (100 * resource1Workers / size).toString() + '%';
		}

		if (mine2Built) {
			document.getElementById('population-resource-2-workers-number').innerHTML = general.formatNumber(resource2Workers);
			document.getElementById('population-resource-2-per-hour').innerHTML = popPicPath2 + ' per hour x ' + general.formatNumber(resource2Workers * popPerHour2);
			resource2Meter.style.width = (100 * resource2Workers / size).toString() + '%';
		}		
	}
	
	$(document).on('click', '#close-population', close);
	
	$(document).on('click', '#population-resource-1-workers-plus', function() {
		if (resource1Workers + resource2Workers + size / 50 <= size) {
			resource1Workers += Math.floor(size / 50);
			updateMeters();
		}
	});
	
	$(document).on('click', '#population-resource-1-workers-minus', function() {
		if (resource1Workers >= size / 50) {
			resource1Workers -= Math.floor(size / 50);
			updateMeters();
		} else {
			resource1Workers = 0;
			updateMeters();
		}
	});
	
	$(document).on('click', '#population-resource-2-workers-plus', function() {
		if (resource1Workers + resource2Workers + size / 50 <= size) {
			resource2Workers += Math.floor(size / 50);
			updateMeters();
		}
	});
	
	$(document).on('click', '#population-resource-2-workers-minus', function() {
		if (resource2Workers >= size / 50) {
			resource2Workers -= Math.floor(size / 50);
			updateMeters();
		} else {
			resource2Workers = 0;
			updateMeters();
		}
	});
	
	$(document).on('click', '#population-save', function() {
		let msgDiv = document.getElementById('population-msg');

		$.ajax({
			url: '/ajax/population',
			dataType: 'json',
			type: 'post',
			contentType: 'application/json',
			data: JSON.stringify({
				resource1: parseInt(resource1Workers),
				resource2: parseInt(resource2Workers) 
			}),
			processData: false,
			success: function(data) {
				msgDiv.style.background = '#28A853';
				msgDiv.innerHTML = 'Changes saved.';
				$('#population-msg').fadeOut();
				$('#population-msg').fadeIn();
			},
			error: function(err) {
				msgDiv.style.background = 'rgb(248, 79, 79)';
				msgDiv.innerHTML = err.responseJSON.error;
				$('#population-msg').fadeOut();
				$('#population-msg').fadeIn();				
			}
		});
	});

	return {
		load: load,
		start: start
	};
})();