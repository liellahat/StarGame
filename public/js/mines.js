let mines = (function() {
	let open = false;

	function start() {
        popUps = [];

        if (open) close();
        else load();
        
        open = !open;
    }

	function load() {
		general.handlePopUps('mines');
		general.load('/ajax/mines', 'mines', function() {
			if (!document.getElementById('mine-1-upgrade')) {
				let waitTime = document.getElementById('wait-1-invisible').innerHTML;
				minesCountDown(document.getElementById('wait-1'), new Date(waitTime));
			}
			if (!document.getElementById('mine-2-upgrade')) {
				let waitTime = document.getElementById('wait-2-invisible').innerHTML;
				minesCountDown(document.getElementById('wait-2'), new Date(waitTime));
			}
		});
	}

	function close() {
		popUps.pop();
		general.closePopUps();
	}

	function minesCountDown(waitTimeDiv, readyTime) {
		var timer = setInterval(function() {
			var time = general.timeDifference(readyTime);
			waitTimeDiv.innerHTML = 'UPGRADING<br>' + time;  
		}, 1000);
	}

	$(document).on('click', '#close-mines', close);

	$(document).on('click', '#mine-1-upgrade', function() {
		$.ajax({
			url: '/ajax/mines/upgrade/1',
			type: 'GET',
			success: function(data) {
				topDiv.updateResources(data.costData);
				load();
			}
		});
	});

	$(document).on('click', '#mine-2-upgrade', function() {
		$.ajax({
			url: '/ajax/mines/upgrade/2',
			type: 'GET',
			success: function(data, textStatus, jQxhr) {
				topDiv.updateResources(data.costData);
				load();
			}
		});
	});

	return {
		load: load,
		start: start
	};
})();