let topDiv = (function() {
    let planetsDropdownOpen = false;
    let resourcesShown = false;

    async function showResources() {
        if (!resourcesShown) {
            resourcesShown = true;
    
            $('#top-resources').height('7vh');
            await general.sleep(200);

            if (resourcesShown) {  // re-check after 0.4 seconds
                $('.top-resource').show();     
            }          
        }
    }

    function hideResources() {
        if (resourcesShown) {
            resourcesShown = false;

            $('#top-resources').height(0);
            $('.top-resource').hide();  
        }
    }
    
    function minimize() {
        $('#top-div')
            .width('7vw')
            .find('[minimizable]').hide();

        $('#top-minimize').hide();
        $('#top-extend').show();
    }

    async function extend() {
        let topDiv = $('#top-div').width('37vw');
        await general.sleep(200);
        $(topDiv).find('[minimizable]').show();

        $('#top-extend').hide();
        $('#top-minimize').show();
    }

    async function openPlanetsDropdown() {
        if (!planetsDropdownOpen) {
            planetsDropdownOpen = true;

            let height = 4 + ($('.top-planets-dropdown-line').length) * 4.75 + 'vh';
    
            $('#top-planets-dropdown').height(height);
            await general.sleep(200);

            if (planetsDropdownOpen) {  // re-check after 0.4 seconds
                $('.top-planets-dropdown-hr').show();
                $('.top-planets-dropdown-line').show();
            }
        }
    }

    async function closePlanetsDropdown() {
        if (planetsDropdownOpen) {
            planetsDropdownOpen = false;

            $('#top-planets-dropdown').height('3vh');
            $('.top-planets-dropdown-hr').hide();
            $('.top-planets-dropdown-line').hide();
        }
    }

    function updateGC(value) {
        gc += value;
        $('#top-gc').html('<img src="/images/icons/gc.png">' + general.formatNumber(gc)); 
    }

    function updateIron(value) {
        iron += value;
        $('#top-resources-iron').html('<img src="/images/icons/iron.png">' + general.formatNumber(iron)); 
    }

    function updateHydrogen(value) {
        hydrogen += value;
        $('#top-resources-hydrogen').html('<img src="/images/icons/iron.png">' + general.formatNumber(hydrogen)); 
    }

    function updateTitanium(value) {
        titanium += value;
        $('#top-resources-titanium').html('<img src="/images/icons/iron.png">' + general.formatNumber(titanium)); 
    }

    function updateUranium(value) {
        uranium += value;
        $('#top-resources-uranium').html('<img src="/images/icons/iron.png">' + general.formatNumber(uranium)); 
    }

    function updateOrium(value) {
        orium += value;
        $('#top-resources-orium').html('<img src="/images/icons/iron.png">' + general.formatNumber(orium)); 
    }

    function updateResources(costData) {
        for (let i = 0; i < costData.length; i++) {
            switch (costData[i].name) {
                case 'gc':
                    updateGC(-1 * costData[i].amount);
                    break;
                case 'iron':
                    updateIron(-1 * costData[i].amount);
                    break;
                case 'hydrogen':
                    updateHydrogen(-1 * costData[i].amount);
                    break;
                case 'titanium':
                    updateTitanium(-1 * costData[i].amount);
                    break;
                case 'uranium':
                    updateUranium(-1 * costData[i].amount);
                    break;
                case 'orium':
                    updateOrium(-1 * costData[i].amount);
                    break;
                default:
                    break;
            }
        }
    }

    return {
        showResources: showResources,
        hideResources: hideResources,
        minimize: minimize,
        extend: extend,
        updateGC: updateGC,
        updateIron: updateIron,
        updateHydrogen: updateHydrogen,
        updateTitanium: updateTitanium,
        updateUranium: updateUranium,
        updateOrium: updateOrium,
        updateResources: updateResources,
        openPlanetsDropdown: openPlanetsDropdown,
        closePlanetsDropdown: closePlanetsDropdown
    }
})();