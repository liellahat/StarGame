//create and set renderer
var renderer = new THREE.WebGLRenderer({canvas: document.getElementById('canvas'), antialias: true});
renderer.setClearColor(0x000000);
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);

var clock = new THREE.Clock();

//create and set camera
var camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 0.1, 3000);  // fov, aspect ratio, near, far

var scene = new THREE.Scene();
document.body.appendChild(renderer.domElement);

//create lights
var light = new THREE.AmbientLight(0xffffff, 0.6);	

// create background stars
createBackground();

requestAnimationFrame(render);
function render() {
	requestAnimationFrame(render);
	renderer.render(scene, camera);
}

function createBackground() {	
	var starsMaterials = [
		new THREE.PointsMaterial( { color: 0x555555, size: 2, sizeAttenuation: false } ),
		new THREE.PointsMaterial( { color: 0x555555, size: 1, sizeAttenuation: false } ),
		new THREE.PointsMaterial( { color: 0x333333, size: 2, sizeAttenuation: false } ),
		new THREE.PointsMaterial( { color: 0x3a3a3a, size: 1, sizeAttenuation: false } ),
		new THREE.PointsMaterial( { color: 0x1a1a1a, size: 2, sizeAttenuation: false } ),
		new THREE.PointsMaterial( { color: 0x1a1a1a, size: 1, sizeAttenuation: false } )
	];

	for (var i = 0; i < 5000; i++)
	{
		var dotGeometry = new THREE.Geometry();
			dotGeometry.vertices.push(new THREE.Vector3(
			Math.floor((Math.random() * 1000) - 450),
			Math.floor((Math.random() * 420) - 210),
			Math.floor((Math.random() * -301) - 500)
		));
		var dot = new THREE.Points( dotGeometry, starsMaterials[Math.floor(Math.random() * 5)] );
		scene.add( dot );
	}
}
