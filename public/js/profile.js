let profile = (function() {
    let open = false;
    let userID;

    function start(id) {
        popUps = [];

        if (open) close();
        else load(id);
        
        open = !open;
    }

    function load(id) {
        general.handlePopUps('profile');
        
        userID = id;

        general.load('/ajax/profile/' + userID, 'profile', function() {
            let militaryPowerDiv = document.getElementById('profile-military-power');
            militaryPowerDiv.innerHTML = 'Total military power: ' + general.formatNumber(militaryPowerDiv.innerHTML.split(': ')[1]);
        });
    }

    function close() {
        popUps.pop();
        general.closePopUps();

        if (popUps[popUps.length - 1] == 'scoreboards') {
            scoreboards.load();
        } else if (popUps[popUps.length - 1] == 'friendlist') {
            friendList.load();
        }
    }

    $(document).on('click', '#close-profile', close);

    // lore
    $(document).on('click', '#profile-lore-button', function() {
        general.handlePopUps('lore');        
        general.load('/ajax/lore/' + userID, 'lore');
    }); 

    $(document).on('click', '#close-lore', function() {
        popUps.pop();

        if (popUps[popUps.length - 1] == 'profile') {
            load(userID);
        }
    });

    // messages
    $(document).on('click', '#profile-make-message-button', function() {
        makeMessage.load(userID);
    });

    return {
        load: load,
        start: start
    }
})();