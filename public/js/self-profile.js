let selfProfile = (function() {
	let open = false;

	function start() {
		popUps = [];

		if (open) close();
		else load();
		
		open = !open;
	}

    function load() {
		general.handlePopUps('self-profile');
		
        general.load('/ajax/profile/self', 'self-profile', function() {
            let militaryPowerDiv = document.getElementById('self-profile-military-power');
			militaryPowerDiv.innerHTML = 'Total military power: ' + general.formatNumber(militaryPowerDiv.innerHTML.split(': ')[1]);
		});
	}

	function close() {
		popUps.pop();	
		general.closePopUps(); 
		
		if (popUps[popUps.length - 1] == 'scoreboards') {
			scoreboards.load();
		} else if (popUps[popUps.lenght - 1] == 'friendlist') {
			friendList.load();
		}
	}
	
	$(document).on('click', '#close-self-profile', close);

    $(document).on('click', '#self-profile-save', function() {
        $.ajax({
            url: '/ajax/profile/self',
			dataType: 'json',
			type: 'post',
			contentType: 'application/json',
			data: JSON.stringify({
                nickname: document.getElementById('self-profile-player-input').value,
                raceName: document.getElementById('self-profile-race-input').value,
				empireName: document.getElementById('self-profile-empire-input').value,
				email: document.getElementById('self-profile-email-input').value
			}),
			processData: false,
			success: function(data) {
				$('#self-profile-msg')
					.html('Changes saved.')
					.css({ 'background': '#28A853' })
					.fadeOut()
					.fadeIn();
			},
			error: function(err) {
				$('#self-profile-msg')
					.html(err.responseJSON.error)
					.css({ 'background': 'rgb(248, 79, 79)' })
					.fadeOut()
					.fadeIn();
			}
        });
	});
	
	// lore
	$(document).on('click', '#self-profile-lore-button', function() {
        general.handlePopUps('self-lore');        
		general.load('/ajax/lore/self', 'lore');
	});

	$(document).on('click', '#close-self-lore', function() {
		popUps.pop();
		general.closePopUps();
		
		if (popUps[popUps.length - 1] == 'self-profile') {
			load();
		}
	});

	$(document).on('click', '#self-lore-save', function() {
		$.ajax({
            url: '/ajax/lore/self',
			dataType: 'json',
			type: 'post',
			contentType: 'application/json',
			data: JSON.stringify({
                lore: document.getElementById('self-lore-box').value
			}),
			processData: false,
			success: function(data) {
				$('#self-lore-msg')
					.html('Changes saved.')
					.css({ 'background': '#28A853' })
					.fadeOut()
					.fadeIn();
			},
			error: function(err) {
				$('#self-lore-msg')
					.html(err.responseJSON.error)
					.css({ 'background': 'rgb(248, 79, 79)' })
					.fadeOut()
					.fadeIn();
			}
        });
	});

    return {
		load: load,
		start: start
    };
})();