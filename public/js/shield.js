let shield = (function() {
	let open = false;

    function start() {
        popUps = [];

        if (open) close();
        else load();
        
        open = !open;
    }

    function load() {
        general.closePopUps();
        general.load('/ajax/shield', 'shield', function() {
			// formatting
			let domeButton = document.getElementById('shield-dome-button');
			if (!domeButton){
				let waitTill = document.getElementById('shield-dome-wait-till').innerHTML;
				let waitDiv = document.getElementById('shield-dome-wait');

				domeCountDown(waitDiv, new Date(waitTill));
			}
			
			let turretsButton = document.getElementById('shield-turrets-button');
			if (!turretsButton) {
				let waitTill = document.getElementById('shield-turrets-wait-till').innerHTML;
				let waitDiv = document.getElementById('shield-turrets-wait');

				turretsCountDown(waitDiv, new Date(waitTill));
			}
		});
	}
	
	function domeCountDown(waitDiv, readyTime) {
        let timer = setInterval(function() {
            let time = general.timeDifference(readyTime);
            waitDiv.innerHTML = 'CONSTRUCTING<br/>' + time;  
        }, 1000);
    }
	
	function turretsCountDown(waitDiv, readyTime) {
        let timer = setInterval(function() {
            let time = general.timeDifference(readyTime);
            waitDiv.innerHTML = 'CONSTRUCTING<br/>' + time;  
        }, 1000);
    }

    $(document).on('click', '#close-shield', function() {
		general.closePopUps();
    });

    $(document).on('click', '#shield-dome-button', function() {
		let action = document.getElementById('shield-dome-button').getAttribute('action');
		
		$.ajax({
            url: '/ajax/shield/dome',
            dataType: 'json',
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify({
				action: action
			}),
			processData: false,
			success: function(data, textStatus, jQxhr) {
				load();
			}
		});
    });

    $(document).on('click', '#shield-turrets-button', function() {
        let action = document.getElementById('shield-turrets-button').getAttribute('action');
        
		$.ajax({
            url: '/ajax/shield/turrets',
            dataType: 'json',
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify({
				action: action
			}),
			processData: false,
			success: function(data, textStatus, jQxhr) {
				load();
			}
		});
    });

	return {
		load: load,
		start: start
	};
})();