let army = (function() {
    let open = false;
    let armyTimers = [];

    function start() {
        popUps = [];

        if (open) close();
        else load();
        
        open = !open;
    }

    function load() {
        general.handlePopUps('army');        
        general.load('/ajax/army', 'army', function() {
            // clear timers
            for (let i = 0; i < armyTimers.length; i++) {
                clearInterval(armyTimers[i]);
            }

            // this time instead of loading data from invisible divs (like in mines, population) we use display=none on the target div and put the data there.
            let waitLines = document.getElementsByClassName('army-unit-line-wait');
            for (let i = 0; i < waitLines.length; i++) {
                let waitTimeDiv = waitLines[i].getElementsByClassName('army-unit-wait')[0];
                let readyTime = new Date(waitTimeDiv.innerHTML);
                armyCountDown(waitTimeDiv, readyTime);
            }
        });
    }

    function close() {
        popUps.pop();
        general.closePopUps(); 
    }

    function armyCountDown(waitTimeDiv, readyTime) {
        let timer = setInterval(function() {
            waitTimeDiv.style.display = 'inline-block';
            let time = general.timeDifference(readyTime);
            waitTimeDiv.innerHTML = time;  
        }, 1000);

        armyTimers.push(timer);
    }

    $(document).on('click', '#close-army', close);

    // dismiss
    let unitCombat;
    let unitCombatSupport;
    let unitGcph;
    let maxAmount; 
    let amount = 0;
    let batch;

    function updateDismiss() {
        document.getElementById('dismiss-amount').value = amount;    
        document.getElementById('dismiss-combat').innerHTML = '<img src="/images/icons/soldier.png"> x ' + general.formatNumber(unitCombat * amount);
        document.getElementById('dismiss-combat-support').innerHTML = '<img src="/images/icons/support.png"> x ' + general.formatNumber(unitCombatSupport * amount);
        document.getElementById('dismiss-gcph').innerHTML = '<img src="/images/icons/gc.png"> per hour x ' + general.formatNumber(unitGcph * amount);
    }

    $(document).on('click', '.army-unit-dismiss', function() {
        id = this.parentElement.id;
        general.load('/ajax/army/dismiss/' + this.parentElement.id, 'dismiss', function() {
            unitCombat = Number(document.getElementById('dismiss-combat-invisible').innerHTML);
            unitCombatSupport = Number(document.getElementById('dismiss-combat-support-invisible').innerHTML);
            unitGcph = Number(document.getElementById('dismiss-gcph-invisible').innerHTML);
            maxAmount = Number(document.getElementById('dismiss-max-amount-invisible').innerHTML);
            batch = Number(document.getElementById('dismiss-batch-invisible').innerHTML);
            amount = 0;
        });
    });

    $(document).on('click', '#close-dismiss', function() {
        general.closePopUp('dismiss'); 
    });

    $(document).on('change', '#dismiss-amount', function() {
        if (this.value < 0) {
            amount = 0;
            updateDismiss();
        } else if (this.value > maxAmount) {
            amount = maxAmount;
            updateDismiss();
        } else if (!isNaN(this.value)) {
            amount = this.value;
            updateDismiss();
        }
    });

    $(document).on('click', '#dismiss-plus', function() {
        let updateValue = amount + batch; 
        if (updateValue < 0) {
            amount = 0;
            updateDismiss();
        } else if (updateValue > maxAmount) {
            amount = maxAmount;
            updateDismiss();
        } else if (!isNaN(updateValue)) {
            amount = updateValue;
            updateDismiss();
        }
    });

    $(document).on('click', '#dismiss-minus', function() {
        let updateValue = amount - batch; 
        if (updateValue < 0) {
            amount = 0;
            updateDismiss();
        } else if (updateValue > maxAmount) {
            amount = maxAmount;
            updateDismiss();
        } else if (!isNaN(updateValue)) {
            amount = updateValue;
            updateDismiss();
        }
    });

    $(document).on('click', '#dismiss-submit', function() {
        if (amount <= 0) return;

        $.ajax({
            url: '/ajax/army/dismiss/' + id + '/' + amount,
            type: 'GET',
            success: function(data) {
                if (data.msg == 'OK') load();         
            }
        });
    });

    return {
        load: load,
        start: start
    };
})();