let development = (function() {
    let open = false;

    function start() {
        popUps = [];

        if (open) close();
        else load();
        
        open = !open;
    }

    function load() {
        general.handlePopUps('development');

        general.load('/ajax/development', 'development', function() {
            let button = document.getElementById('development-upgrade');

            if (!button) {
                let waitDiv = document.getElementById('development-wait');
                let waitTill = document.getElementById('development-wait-till').innerHTML;
                developmentCountDown(waitDiv, new Date(waitTill));
            }
        });
    }

    function close() {
        popUps.pop();
        general.closePopUps();     
    }

    function developmentCountDown(waitDiv, readyTime) {
        var timer = setInterval(function() {
            var time = general.timeDifference(readyTime);
            waitDiv.innerHTML = 'UPGRADING<br/>' + time;  
        }, 1000);
    }

    $(document).on('click', '#close-development', close);

    $(document).on('click', '#development-upgrade', function() {
		$.ajax({
			url: '/ajax/development/upgrade',
			type: 'GET',
			success: function(data) {
                topDiv.updateResources(data.costData);
				load();
			}
		})
    });

    return {
        load: load,
        start: start
    };
})();