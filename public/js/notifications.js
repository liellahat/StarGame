let notifications = (function () {  
    let open = false;

    async function start() {
        popUps = [];

        if (open) await close();
        else load();
        
        open = !open;
    }
    
    function load() {
        $('#notifications').show().width('20vw');

        general.load('/ajax/notifications', 'notifications', function() {
            general.removeNotificationsAlert();;

            if ($('.notifications-line').length == 0) {
                $('#notifications-empty').show();
            } 
        });
    }
 
    async function close() {
        let notifDiv = $('#notifications');
        notifDiv.width(0);
        await general.sleep(400);  // hide after the thing gets shorter (takes 0.4 seconds)
        notifDiv.hide();
    }

    $(document).on('mouseup', async function(e) {    
        let notifDiv = $('#notifications');    
        if (open &&
            !$('#top-notifications-button').is(e.target) &&
            !notifDiv.is(e.target)) {

            await close();
            open = !open;
        }
    });

    return {
        start: start,
        load: load
    };
})();