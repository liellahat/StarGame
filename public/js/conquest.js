let conquest = (function() {
    let open = false;

    let units = { };
    let planetsOpened = new Set();

    let requiredMilitaryPower;
    let militaryPower = 0;
    let militaryPowerDiv;

    let combat = 0;
    let combatSupport = 0;

    function start() {
        popUps = [];

        if (open) close();
        else load();
        
        open = !open;
    }

    function load() {
        general.handlePopUps('conquest');
        general.load('/ajax/conquest/', 'conquest', function() {
            // slicing the required military power from the div
            requiredMilitaryPower = general.midString(
                $('#conquest-required-military-power').html().split('<br>')[0],
                '<div>', '</div>'
            );

            militaryPowerDiv = $('#conquest-military-power');
        });
    }

    function close() {
        popUps.pop();
        general.closePopUps();     
    }

    function handleButton() {
        let button = $('#conquest-conquer');
        
        // if there is enough military power enable the button.
        if (militaryPower > requiredMilitaryPower) {
            $(button).prop('disabled', false);
        }
        else {
            $(button).prop('disabled', true);
        }
    }

    $(document).on('click', '#close-conquest', close);

    $(document).on('click', '.conquest-planet-line', function() {
        let planetID = $(this).attr('id').split('-')[1];

        if ($(this).prop('extended')) {
            $(this).prop('extended', false);
            $('#planet-' + planetID + '-units').html('');
        } else {
            $(this).prop('extended', true);
            general.load('/ajax/conquest/planet-units/' + planetID, 'planet-' + planetID + '-units', function() {
                let planetData = $('#planet-' + planetID + '-units');

                if (!planetsOpened.has(planetID)) {  // if the planet wasn't opened before
                    planetsOpened.add(planetID);
                    let planetUnits = $(planetData).find('.conquest-unit-line');

                    for (let i = 0; i < planetUnits.length; i++) {
                        let id = $(planetUnits[i]).attr('id').split('-')[1];
                        let attack = $(planetUnits[i]).find('.conquest-unit-line-unit-attack').html();
                        let defense = $(planetUnits[i]).find('.conquest-unit-line-unit-defense').html();
                        let combat = $(planetUnits[i]).find('.conquest-unit-line-unit-combat').html();
                        let combatSupport = $(planetUnits[i]).find('.conquest-unit-line-unit-combat-support').html();
                        let limit = $(planetUnits[i]).find('.conquest-unit-line-amount-input').attr('max');
                        
                        units[id] = {
                            unitAttack: attack,
                            unitDefense: defense,
                            unitCombat: combat,
                            unitCombatSupport: combatSupport,
                            limit: limit,
                            deployedAmount: 0
                        }
                    }
                } else {
                    let planetUnits = $(planetData).find('.conquest-unit-line');

                    for (let i = 0; i < planetUnits.length; i++) {
                        let id = $(planetUnits[i]).attr('id').split('-')[1];                        
                        $(planetUnits[i]).find('.conquest-unit-line-amount-input').val(units[id].deployedAmount);
                    }
                }
            });
        }
    });

    $(document).on('keyup mouseup change', '.conquest-unit-line-amount-input', function() {
        let amount = parseInt(this.value);
        let id = $(this).parent().parent().attr('id').split('-')[1];

        if (amount > units[id].limit) {
            amount = units[id].limit;
        }

        if (amount < 0 || isNaN(amount)) {
            amount = 0;
        }

        this.value = amount;

        // updating military power, combat and combat support counters.
        militaryPower -= units[id].deployedAmount * units[id].unitAttack;
        militaryPower -= units[id].deployedAmount * units[id].unitDefense;
        combat -= units[id].deployedAmount * units[id].unitCombat;
        combatSupport -= units[id].deployedAmount * units[id].unitCombatSupport;
        

        units[id].deployedAmount = amount;

        militaryPower += amount * units[id].unitAttack;
        militaryPower += amount * units[id].unitDefense;
        combat += amount * units[id].unitCombat;
        combatSupport += amount * units[id].unitCombatSupport;

        handleButton();

        $(militaryPowerDiv).html('<div>' + general.formatNumber(militaryPower) + '</div><br/>Military power');

        // updating atk def
        $(this).parent().parent().find('.conquest-unit-line-attack').html(general.formatNumber(amount * units[id].unitAttack) + '<br/>ATK');
        $(this).parent().parent().find('.conquest-unit-line-defense').html(general.formatNumber(amount * units[id].unitDefense) + '<br/>DEF');
        
        $('#conquest-combat').html('COMBAT: ' + general.formatNumber(combat));
        $('#conquest-combat-support').html('COMBAT SUPPORT: ' + general.formatNumber(combatSupport));
    });

    $(document).on('click', '#conquest-conquer', function() {
        let postData = [];

        for (let i in units) {
            let amount = units[i].deployedAmount;
            if (amount > 0) {
                postData.push({
                    unitID: i,
                    amount: amount
                }); 
            }
        }     

        $.ajax({
            url: '/ajax/conquest/',
			dataType: 'json',
			type: 'post',
			contentType: 'application/json',
			data: JSON.stringify({ units: postData }),
			processData: false,
			success: function(data) {
                location.reload();
            },
            error: function(err) {
				$('#conquest-msg')
					.html(err.responseJSON.error)
					.fadeOut()
					.fadeIn();
            }
        });
    });

    return {
        start: start,
        load: load
    };
})();