var renderer = new THREE.WebGLRenderer({canvas: document.getElementById('canvas'), antialias: true});
renderer.setClearColor(0x000000);
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);

var camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 0.1, 10000);  // fov, aspect ratio, near, far
camera.position.set(0, 0, 1200);

var scene = new THREE.Scene();
document.body.appendChild(renderer.domElement);

var light = new THREE.AmbientLight(0xffffff, 1.05);
scene.add(light);

var objects = [];

controls = new THREE.OrbitControls(camera);

document.addEventListener('click', onDocumentMouseClick, false);

controls.addEventListener('change', function() {	
	document.getElementById('galaxyX').value = Math.floor(Number(camera.position.x));
	document.getElementById('galaxyY').value = Math.floor(Number(camera.position.y));	
	renderer.render(scene, camera);
});

controls.mouseButtons = {
	ORBIT: undefined,
	ZOOM: undefined,
	PAN: THREE.MOUSE.LEFT
}
controls.screenSpacePanning = true;
//controls.enableZoom = false;
controls.enableRotate = false;

render();
CreateBackground();
CreateSolarSystems();

$('#galaxy-jump').click(function(){	
    var x = Number(document.getElementById('galaxyX').value);
    var y = Number(document.getElementById('galaxyY').value);    
    camera.position.set(x, y, 1200);
    controls.target.set(x, y, 0);
    render();
});

function render() {	
    requestAnimationFrame(render);    
	renderer.render(scene, camera);
}

function disableControls(){
	controls.enabled = false;	
}

function enableControls(){
	controls.enabled = true;	
}
function onDocumentMouseClick(event) {
	event.preventDefault();
	var mouse = new THREE.Vector2();
	var projector = new THREE.Projector();
	mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
	mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
	var vector = new THREE.Vector3(mouse.x,mouse.y, 0.5);
	projector.unprojectVector(vector, camera);
	var raycaster = new THREE.Raycaster(camera.position, vector.sub(camera.position).normalize());
	var intersects = raycaster.intersectObjects(objects);
	if (intersects.length > 0) {
		//window.location.replace('http://localhost:9292/solar-system')
	}
}

function CreateSolarSystems(){
	var y = -3000;
	var x = -6000;
	var texture = new THREE.TextureLoader().load("/images/textures/sun.png");
	for (var j = 1; j <= 400; j++) {
		var sun = new THREE.Mesh(
			new THREE.SphereGeometry(20, 20),
			new THREE.MeshLambertMaterial({
				map: texture
			})
        )        
		if (j % 20 == 0){
			x = -6000;
			y += 400;
		}
		x += 400 + Math.floor((Math.random() * 1500));		
		sun.position.set(x, y, -1000);
		scene.add(sun);
		objects.push(sun);
		var num_orbits = 3;
		var firstRadius = 30; 

		for (var i = 0; i < num_orbits; i++) {
			var radius = firstRadius + i * 10;
			var geometry = new THREE.CircleGeometry(radius, 25);
			var material = new THREE.MeshLambertMaterial({color: 0xffffff, wireframe: true});
			var circle = new THREE.Mesh(geometry, material);
			circle.rotation.set(0.5,0.1,0.1);
			circle.position.set(sun.position.x, sun.position.y, sun.position.z);
			scene.add(circle);
		}
	}
}
/*
if (Math.floor(Number(camera.position.y)) <= -2780 || Math.ceil(Number(camera.position.y)) <= -2780) {
		camera.position.y = -2778;
		console.log('heloo');
	}
	*/

function CreateBackground(){
	var x = -21000;
    var y = -21000;
    
	for (var j = 0; j <= 5000; j++) {
		var geometry = new THREE.PlaneGeometry(720, 720);
		var material = new THREE.MeshBasicMaterial({
			map: new THREE.TextureLoader().load("/images/background2.jpg"), 
			side: THREE.DoubleSide
		});
		var plane = new THREE.Mesh(geometry, material);
		x += 720;
		if (j % 100 == 0){
			x = -21000;
			y += 720;
		}
		plane.rotation.z = (((j%5)/2) * Math.PI);
		plane.position.set(x, y, -2000);
		scene.add(plane);
	}
}
function CreateBackground2(){	
	var geometry = new THREE.PlaneGeometry(720, 720);
	var material = new THREE.MeshBasicMaterial({
		map: new THREE.TextureLoader().load("/images/background.jpg"), 
		side: THREE.DoubleSide
	});
	plane.position.set(x, y, -2000);
	scene.add(plane);
}
function CreateBackground2(){
	var x = -18500;
    var y = -15000;
    
	for (var j = 0; j <= 1674; j++) {
		var geometry = new THREE.PlaneGeometry(720, 720);
		var material = new THREE.MeshBasicMaterial({
			map: new THREE.TextureLoader().load("/images/background2.jpg"), 
			side: THREE.DoubleSide
		});
		var plane = new THREE.Mesh(geometry, material);
		x += 720;
		if (j % 45 == 0){
			x = -18500;
			y += 720;
		}
		plane.rotation.z = (((j%5)/2) * Math.PI);
		plane.position.set(x, y, -2000);
		scene.add(plane);
	}
}
function CreateBackground2(){	
	var geometry = new THREE.PlaneGeometry(720, 720);
	var material = new THREE.MeshBasicMaterial({
		map: new THREE.TextureLoader().load("/images/background.jpg"), 
		side: THREE.DoubleSide
	});
	plane.position.set(x, y, -2000);
	scene.add(plane);
}