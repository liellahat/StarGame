var renderer = new THREE.WebGLRenderer({canvas: document.getElementById('canvas'), antialias: true});
renderer.setClearColor(0x000000);
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
var clock = new THREE.Clock();

var camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 0.1, 100000);  // fov, aspect ratio, near, far
camera.position.set(970, -862, 680);
camera.rotation.set(0.44, 0.66, 0.07);
var scene = new THREE.Scene();
document.body.appendChild(renderer.domElement);

var light = new THREE.AmbientLight(0xffffff, 0.6);
scene.add(light);

var num_planets = 7;

var light1 = new THREE.PointLight(0xffffff, 1);
light1.position.set(0, 0, -1000 );
scene.add(light1);

var light2 = new THREE.PointLight(0xffffff, 1);
light2.position.set(500, 500, -1000 );
scene.add(light2);

var sun = new THREE.Mesh(
	new THREE.IcosahedronBufferGeometry(100, 3),
	new THREE.MeshPhongMaterial({
		color: 0xffff00,
		flatShading: true
	})
);	

sun.position.set(0, 0, -1000);
scene.add(sun);

var planets = new Hashtable();

var firstRadius = 150; 
for (var i = 0; i < num_planets; i++) {
	var planet = new THREE.Mesh(
		new THREE.IcosahedronBufferGeometry(20, 0),
		new THREE.MeshPhongMaterial({
			color: 0xffffaa,
			flatShading: true
		})
	);
	
	var radius = firstRadius + i * 80;
	
	var geometry = new THREE.CircleGeometry(radius, 50);
	var material = new THREE.MeshLambertMaterial({color: 0xffffff, wireframe: true});
	var circle = new THREE.Mesh(geometry, material);
	circle.position.set(0,0, -1000);
	scene.add(circle);	
	
	var x = Math.floor((Math.random() * radius * 2) - radius);
	var down = Math.floor(Math.random() * 2);
	var y = Math.floor(Math.sqrt(radius * radius - x * x));
	if (down) {
		y = -y;
	}
	console.log(x);
	console.log(y);
	
	planet.position.set(x, y, -1000);
	scene.add(planet);
	planets.put(planet, [radius, Math.tan(y / x), 0.001 * (num_planets - i)]);
}

var starsMaterials = [
	new THREE.PointsMaterial({ color: 0x555555, size: 2, sizeAttenuation: false }),
	new THREE.PointsMaterial({ color: 0x555555, size: 1, sizeAttenuation: false }),
	new THREE.PointsMaterial({ color: 0x333333, size: 2, sizeAttenuation: false }),
	new THREE.PointsMaterial({ color: 0x3a3a3a, size: 1, sizeAttenuation: false }),
	new THREE.PointsMaterial({ color: 0x1a1a1a, size: 2, sizeAttenuation: false }),
	new THREE.PointsMaterial({ color: 0x1a1a1a, size: 1, sizeAttenuation: false })
];

for (var i = 0; i < 900; i++) {
	var dotGeometry = new THREE.Geometry();
		dotGeometry.vertices.push(new THREE.Vector3(
		Math.floor((Math.random() * 4800) - 6000),
		Math.floor((Math.random() * 3000) - 1300),
		Math.floor((Math.random() * -301) - 1000)
	));
	var dot = new THREE.Points( dotGeometry, starsMaterials[Math.floor(Math.random() * 5)] );
	scene.add( dot );
}	

for (var i = 0; i < 1000; i++) {
	var dotGeometry = new THREE.Geometry();
		dotGeometry.vertices.push(new THREE.Vector3(
		Math.floor((Math.random() * 1500) - 1200),
		Math.floor((Math.random() * 3000) - 1300),
		Math.floor((Math.random() * -301) - 1000)
	));
	var dot = new THREE.Points( dotGeometry, starsMaterials[Math.floor(Math.random() * 5)] );
	scene.add( dot );
}	

for (var i = 0; i < 1800; i++) {
	var dotGeometry = new THREE.Geometry();
		dotGeometry.vertices.push(new THREE.Vector3(
		Math.floor((Math.random() * 1300) + 300),
		Math.floor((Math.random() * 3000) - 1300),
		Math.floor((Math.random() * -301) - 1000)
	));
	var dot = new THREE.Points( dotGeometry, starsMaterials[Math.floor(Math.random() * 5)] );
	scene.add( dot );
}			

/*controls = new THREE.FlyControls(camera);			
controls.movementSpeed = 3000;
controls.domElement = renderer.domElement;
controls.rollSpeed = Math.PI / 10;
controls.autoForward = false;
controls.dragToLook = true;
var clock = new THREE.Clock();*/

requestAnimationFrame(render);
function render() {
	//var delta = clock.getDelta();
	sun.rotation.x += 0.001;
	planets.each(function(planet, value) {
		planet.position.set(value[0] * Math.cos(value[1]), value[0] * Math.sin(value[1]), -1000);
		value[1] += value[2];
	});
	//controls.update(delta);
	renderer.render(scene, camera);
	requestAnimationFrame(render);	
}