let friendList = (function() {
    let open = false;
    let requests;

    function start() {
        popUps = [];

        if (open) close();
        else load();
        
        open = !open;
    }
    
    function load() {
        general.handlePopUps('friendlist');

        general.load('/ajax/friends/list', 'friendlist', function() {
            requests = $('.pending-request').length;
        });
    }

    function close() {
        popUps.pop();
        general.closePopUps();     
    }

    $(document).on('click', '#close-friendlist', close);

    // add friend
    $(document).on('click', '#friendlist-add-button', function() {
        $.ajax({
            url: '/ajax/friends/add/nickname/' + document.getElementById('friendlist-add-friend-input').value,
            type: 'GET',
            success: function(data) {
                load();
            },
            error: async function(err) {
                let msgDiv = $('#friendlist-msg-div');
                $(msgDiv).html(err.responseJSON.error);
                $(msgDiv).fadeOut();
                $(msgDiv).fadeIn();                
                await general.sleep(3000);
                $(msgDiv).fadeOut();
            }
        });
    });

    $(document).on('click', '.friendlist-line', function() {
        let userID = $(this).attr('userID'); 
        profile.load(userID);
    });

    // x button
    $(document).on('click', '.friendlist-line-x', function(event) {
        event.stopPropagation();

        requests--;
        if (requests == 0) {
            general.removeFriendlistAlert();
        }

        let classes = $(this).parent().attr('class');
        let id = $(this).parent().attr('userID');
        
        if (classes.includes('outgoing-request')) {
            $.ajax({
                url: '/ajax/friends/request/' + id + '/cancel',
                type: 'GET',
                success: function(data, status, jQxhr) {
                    load();
                }
            });
        } else if (classes.includes('pending-request')) {
            $.ajax({
                url: '/ajax/friends/request/' + id + '/decline',
                type: 'GET',
                success: function(data, status, jQxhr) {
                    load();
                }
            });
        } else if (classes.includes('friend')) {
            $.ajax({
                url: '/ajax/friends/remove/' + id,
                type: 'GET',
                success: function(data, status, jQxhr) {
                    load();
                }
            });
        }
    });

    // v button
    $(document).on('click', '.friendlist-line-v', function(event) {
        event.stopPropagation();

        requests--;
        if (requests == 0) {
            general.removeFriendlistAlert();
        }
        
        let id = $(this).parent().attr('userID');

        $.ajax({
            url: '/ajax/friends/request/' + id + '/accept',
            type: 'GET',
            success: function(data, status, jQxhr) {
                load();
            }
        }); 
    });

    return {
        start: start,
        load: load
    };
})();