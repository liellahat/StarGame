let messages = (function() {
    let open = false;
    let unreadCount;

    // messages (inbox outbox)
    function start() {
        popUps = [];

        if (open) close();
        else load('inbox');
        
        open = !open;
    }

    function load(mailbox) {
        general.handlePopUps('messages');
        general.load('/ajax/messages/' + mailbox, 'messages', function() {
            let dates = document.getElementsByClassName('message-line-time');
    
            for (let i = 0; i < dates.length; i++) {
                dates[i].innerHTML = new Date(dates[i].innerHTML).toLocaleDateString();
            }

            unreadCount = $('.unread').length;
        });
    }

    function close() {
        popUps.pop();
        general.closePopUps();     
    }

	$(document).on('click', '#close-messages', close);

    // inbox and outbox    
    $(document).on('click', '#messages-inbox', function() {
        load('inbox');
    });

    $(document).on('click', '#messages-outbox', function() {
        load('outbox');
    });

    // new message
    $(document).on('click', '#messages-new-message', function() {
        makeMessage.load();
    });

    // open message
    $(document).on('click', '.message-inbox-line', function() {
        if (this.className.includes('unread')) {
            unreadCount--;
            if (unreadCount == 0) {
                general.removeMessagesAlert();
            }
        }

        popUps.push('message');
        general.closePopUps();
        general.load('/ajax/messages/inbox/' + this.id, 'message');   
    });

    $(document).on('click', '.message-outbox-line', function() {
        popUps.push('message');
        general.closePopUps();
        general.load('/ajax/messages/outbox/' + this.id, 'message');         
    });

    // respond
    $(document).on('click', '#message-respond', function() {
        let msgID = document.getElementById('message-id').innerHTML;
        makeMessage.loadResponse(msgID);
    });

    // close message
	$(document).on('click', '#close-message', function() {
        popUps.pop();

        if (popUps[popUps.length - 1] == 'messages') {
            load('inbox');
        }        
    });

    return {
        load: load,
        start: start
    }
})(); 