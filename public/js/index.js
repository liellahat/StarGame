let index = (function() {
    let open = false;

    function start() {
        popUps = [];

        if (open) close();
        else load();
        
        open = !open;
    }

    function load() {
        general.handlePopUps('index');
        general.load('/ajax/index', 'index');
    }

    function close() {
        popUps.pop();
        general.closePopUps();     
    }

    $(document).on('click', '#close-index', close);

    // classes
    $(document).on('click', '#index-military-classes', function() {
        general.handlePopUps('classes');
        general.load('/ajax/index/classes', 'classes');
    });

    $(document).on('click', '#close-classes', function() {
        popUps.pop();
        load();
    });

    // resources
    $(document).on('click', '#index-resources', function() {
        general.handlePopUps('resources');
        general.load('/ajax/index/resources', 'resources');
    });

    $(document).on('click', '#close-resources', function() {
        popUps.pop();
        load();
    });

    return {
        start: start,
        load: load
    };
})();