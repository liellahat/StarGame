let makeMessage = (function() {
    let open = false;
    let userID;

    function start(id) {
        popUps = [];

        if (open) close();
        else load(id);
        
        open = !open;
    }

    function load(id) {
        general.handlePopUps('make-message');

        userID = id;

        if (userID) {
            general.load('/ajax/messages/make-message/' + userID, 'make-message');
        }
        else {
            general.load('/ajax/messages/make-message', 'make-message');
        }         
    }

    function loadResponse(msgID) {
        popUps.push('make-response');
        general.closePopUps();
        
        general.load('/ajax/messages/make-response/' + msgID, 'make-message');
    }

    function close() {
        popUps.pop();
        general.closePopUps();

        if (popUps[popUps.length - 1] == 'profile') {
            profile.load(userID);
        } else if (popUps[popUps.length - 1] == 'message') {
            popUps.pop();  // skip message
            messages.load('inbox');
        } else if (popUps[popUps.length - 1] == 'messages') {
            messages.load('inbox');
        }
    }

	$(document).on('click', '#close-make-message', close);

    $(document).on('click', '#make-message-send', function() {
        let recipient = document.getElementById('make-message-recipient-input').value.trim();
        let topic = document.getElementById('make-message-topic-input').value.trim();
        let message = document.getElementById('make-message-message-textarea').value;

        let msgDiv = document.getElementById('make-message-msg');

        $.ajax({
            url: '/ajax/messages/make-message',
			dataType: 'json',
			type: 'post',
			contentType: 'application/json',
			data: JSON.stringify({
                recipient: recipient,
                topic: topic,
                message: message
			}),
			processData: false,
			success: function(data) {      
                msgDiv.style.background = '#28A853';
                msgDiv.innerHTML = 'Message sent.';
                $(msgDiv).fadeOut();
                $(msgDiv).fadeIn();
            },
            error: function(err) {
                msgDiv.style.background = 'rgb(248, 79, 79)';
                msgDiv.innerHTML = err.responseJSON.error;
                $(msgDiv).fadeOut();
                $(msgDiv).fadeIn();
            }
        });    
    });

    return {
        load: load,
        loadResponse: loadResponse,
        start: start
    };
})(); 