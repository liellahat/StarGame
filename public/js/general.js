let general = (function() {
	function formatNumber(num) {
		if (typeof(num) == 'string') {
			num = parseInt(num);
		}

		if (num > 10000000000000) return Math.round(num / 1000000000000 * 10) / 10 + 'Q';
		if (num > 10000000000) return Math.round(num / 1000000000 * 10) / 10 + 'B';
		if (num > 10000000) return Math.round(num / 1000000 * 10) / 10 + 'M'; 
		if (num > 10000) return Math.round(num / 1000 * 10) / 10 + 'K';

		return Math.round(num * 10) / 10;
	}
	
	function formatDate(date) {
		var year = date.getUTCFullYear().toString();
		var month = (date.getUTCMonth() + 1).toString();
		var day = date.getUTCDate().toString();
		var hour = date.getUTCHours().toString();
		var minute = date.getUTCMinutes().toString();
		var second = date.getUTCSeconds().toString(); 
		
		var y = year.length < 4 ? '0'.repeat(4 - year.length) + year : year;
		var m = month.length < 2 ? '0'.repeat(2 - month.length) + month : month;
		var d = day.length < 2 ? '0'.repeat(2 - day.length) + day : day;
		var H = hour.length < 2 ? '0'.repeat(2 - hour.length) + hour : hour;
		var M = minute.length < 2 ? '0'.repeat(2 - minute.length) + minute : minute;
		var S = second.length < 2 ? '0'.repeat(2 - second.length) + second : second;
		
		return [y, m, d].join('-') + ' ' + [H, M, S].join(':');
	}
	
	function timeDifference(waitTime, onFinish) {
		//var now = new Date(formatDate(new Date()));  // kind of lazy, will be fixed later
		var now = new Date();
		var distance = waitTime - now;
		var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		var seconds = Math.floor((distance % (1000 * 60)) / 1000);
	
		var daysStr = '';
		var hoursStr = '';
		var minutesStr = '';
	
		if (distance < 0) {
			if (onFinish) {
				onFinish();
			} else {
				return '0S';
			}
		} else {
			if (days) {
				daysStr = days + 'D ';
			}
			if (hours) {
				hoursStr = hours + 'H ';
			}
			if (minutes) {
				minutesStr = minutes + 'M ';
			}
	
			return daysStr + hoursStr + minutesStr + seconds + 'S';	
		}
	}
	
	function closePopUps(next) {
		var elements = document.getElementsByClassName('pop-up');
		
		for (var i = 0; i < elements.length; i++) {
			elements[i].innerHTML = '';
			elements[i].style.display = 'none';		
		}
	
		if (next) {
			next();
		}
	}

	function closePopUp(id, next) {
        var element = document.getElementById(id);
        element.innerHTML = ''; 
		element.style.display = 'none';
		
		if (next) {
			next();
		}
	}
	
	function handlePopUps(popUp) {
		general.closePopUps();

		if (popUps.includes(popUp)) {
			for (let i = popUps.length - 1; popUps[i] != popUp; i--) popUps.pop();
		} else {
			popUps.push(popUp);
		}
	}

	function load(path, targetId, callback) {
		let xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				let element = document.getElementById(targetId);
				element.innerHTML = this.responseText;
				element.style.display = 'block';			
				if (callback) {
					callback();
				}
			}
		};
		xhttp.open("GET", path, true);
		xhttp.send();
	}

	function sleep(ms) {
		return new Promise(function(resolve) {
			setTimeout(resolve, ms);
		});
	}

	function redirect(path) {
		window.location = path;
	}

	// returns the part of the string between the two given strings.
	function midString(str, s1, s2) {
		return str.substring(str.lastIndexOf(s1) + s1.length, str.lastIndexOf(s2));
	}

	function hasAttribute(obj, attrName) {
		let attr = $(obj).attr(attrName);

		return typeof attr !== typeof undefined && attr !== false;
	}

	function removeMessagesAlert() {
		$('#top-messages-button').html('MESSAGES');
	}

	function removeFriendlistAlert() {
		$('#top-friendlist-button').html('FRIEND LIST');
	}

	function removeNotificationsAlert() {
		$('#top-notifications-button').html('NOTIFICATIONS');
	}
	
	$(document).ready(function() {
		var elements = document.getElementsByClassName('pop-up');
	
		for (var i = 0; i < elements.length; i++) {
			$('#' + elements[i].id).draggable({ 'scroll': false });
		}
	});

	return {
		formatNumber: formatNumber,
		formatDate: formatDate,
		timeDifference: timeDifference,
		closePopUps: closePopUps,
		closePopUp: closePopUp,
		handlePopUps: handlePopUps,
		sleep: sleep,
		redirect: redirect,
		load: load,
		midString: midString,
		hasAttribute: hasAttribute,
		removeMessagesAlert: removeMessagesAlert,
		removeFriendlistAlert: removeFriendlistAlert,
		removeNotificationsAlert: removeNotificationsAlert
	};
})();